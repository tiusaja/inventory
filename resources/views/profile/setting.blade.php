@extends('layouts.template')
@section('content')
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
    <link rel="stylesheet" href="style.css">
    <title>Ubah Profile</title>
    <style>
        .image {
            text-align: center;
        }
    </style>
</head>

<body>
    <div class="row">
        <div class="col-md-12 grid-margin">
            <div class="d-flex justify-content-between flex-wrap">
                <div class="d-flex align-items-end flex-wrap">
                    <div class="mr-md-3 mr-xl-5">
                        <h2>{{ $title }}</h2>
                        <div class="d-flex">
                            <i class="mdi mdi-home text-muted hover-cursor"></i>
                            <p class="text-muted mb-0 hover-cursor">&nbsp;/&nbsp;Inventory&nbsp;/&nbsp;</p>
                            <p class="text-primary mb-0 hover-cursor">Data Jenis Asset</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="row">
            <div class="col-md-6">
                <div class="card">
                    <div class="card-body">
                        <h4 class="card-title"></h4>
                        <form class="forms-sample">
                            <div class="image">
                                <img src="{{ asset('assets/images/avatar.jpg') }} " width="150" alt="logo" />
                            </div>
                            <tr>
                                <td colspan="2">
                                    <hr>
                                </td>
                            </tr>
                            <h6><svg xmlns="http://www.w3.org/2000/svg" width="20" height="20" fill="currentColor" class="bi bi-calendar-date" viewBox="0 0 16 16">
                                    <path d="M6.445 11.688V6.354h-.633A12.6 12.6 0 0 0 4.5 7.16v.695c.375-.257.969-.62 1.258-.777h.012v4.61h.675zm1.188-1.305c.047.64.594 1.406 1.703 1.406 1.258 0 2-1.066 2-2.871 0-1.934-.781-2.668-1.953-2.668-.926 0-1.797.672-1.797 1.809 0 1.16.824 1.77 1.676 1.77.746 0 1.23-.376 1.383-.79h.027c-.004 1.316-.461 2.164-1.305 2.164-.664 0-1.008-.45-1.05-.82h-.684zm2.953-2.317c0 .696-.559 1.18-1.184 1.18-.601 0-1.144-.383-1.144-1.2 0-.823.582-1.21 1.168-1.21.633 0 1.16.398 1.16 1.23z" />
                                    <path d="M3.5 0a.5.5 0 0 1 .5.5V1h8V.5a.5.5 0 0 1 1 0V1h1a2 2 0 0 1 2 2v11a2 2 0 0 1-2 2H2a2 2 0 0 1-2-2V3a2 2 0 0 1 2-2h1V.5a.5.5 0 0 1 .5-.5zM1 4v10a1 1 0 0 0 1 1h12a1 1 0 0 0 1-1V4H1z" />
                                </svg> Tanggal Daftar : <td></td>
                            </h6>
                            <td colspan="2">
                                <hr>
                            </td>
                            <h6><svg xmlns="http://www.w3.org/2000/svg" width="20" height="20" fill="currentColor" class="bi bi-watch" viewBox="0 0 16 16">
                                    <path d="M8.5 5a.5.5 0 0 0-1 0v2.5H6a.5.5 0 0 0 0 1h2a.5.5 0 0 0 .5-.5V5z" />
                                    <path d="M5.667 16C4.747 16 4 15.254 4 14.333v-1.86A5.985 5.985 0 0 1 2 8c0-1.777.772-3.374 2-4.472V1.667C4 .747 4.746 0 5.667 0h4.666C11.253 0 12 .746 12 1.667v1.86a5.99 5.99 0 0 1 1.918 3.48.502.502 0 0 1 .582.493v1a.5.5 0 0 1-.582.493A5.99 5.99 0 0 1 12 12.473v1.86c0 .92-.746 1.667-1.667 1.667H5.667zM13 8A5 5 0 1 0 3 8a5 5 0 0 0 10 0z" />
                                </svg> Login Terakhir : <td></td>
                            </h6>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <div class="row d-flex justify-content-end" style="margin-top: -345px; margin-bottom :130px;">
            <div class="col-md-6">
                <div class="card">
                    <div class="card-body">
                        <h4 class="card-title"><svg xmlns="http://www.w3.org/2000/svg" width="20" height="20" fill="currentColor" class="bi bi-person" viewBox="0 0 16 16">
                                <path d="M8 8a3 3 0 1 0 0-6 3 3 0 0 0 0 6zm2-3a2 2 0 1 1-4 0 2 2 0 0 1 4 0zm4 8c0 1-1 1-1 1H3s-1 0-1-1 1-4 6-4 6 3 6 4zm-1-.004c-.001-.246-.154-.986-.832-1.664C11.516 10.68 10.289 10 8 10c-2.29 0-3.516.68-4.168 1.332-.678.678-.83 1.418-.832 1.664h10z" />
                            </svg> Ubah Profile</h4>
                        <tr>
                            <td colspan="1">
                                <hr>
                            </td>
                        </tr>
                        <form class="forms-sample">
                            <div class="row mb-2">
                                <label for="namapengguna" class="col-sm-4 col-form-label">Nama Pengguna</label>
                                <div class="col-sm-7">
                                    <input type="text" class="form-control" id="namapengguna">
                                </div>
                            </div>
                            <div class="row mb-2">
                                <label for="namapengguna" class="col-sm-4 col-form-label">Nama Lengkap</label>
                                <div class="col-sm-7">
                                    <input type="text" class="form-control" id="namapengguna">
                                </div>
                            </div>
                            <div class="row mb-2">
                                <label for="email" class="col-sm-4 col-form-label">Email</label>
                                <div class="col-sm-7">
                                    <input type="email" class="form-control" id="email">
                                </div>
                            </div>
                            <div class="row mb-2">
                                <label for="level" class="col-sm-4 col-form-label">Level</label>
                                <div class="col-sm-7">
                                    <input type="level" class="form-control" id="level">
                                </div>
                            </div>
                            <div class="row mb-2">
                                <label for="alamat" class="col-sm-4 col-form-label">Alamat</label>
                                <div class="col-sm-7">
                                    <textarea class="form-control" placeholder="Alamat" id="alamat"></textarea>
                                </div>
                            </div>
                            <div class="row mb-2">
                                <label for="Telepon" class="col-sm-4 col-form-label">Telepon</label>
                                <div class="col-sm-7">
                                    <input type="Integer" class="form-control" id="telpon">
                                </div>
                            </div>
                            <div class="button" style="margin-left : 400px">
                                <button type="submit" class="btn btn-primary btn-sm">Submit</button>
                                <button type="submit" class="btn btn-danger btn-sm">Cancel</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</body>

</html>


@endsection