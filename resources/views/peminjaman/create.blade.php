@extends('layouts.template')
@section('content')
    <div class="row">
        <div class="col-md-12 grid-margin">
            <div class="d-flex justify-content-between flex-wrap">
                <div class="d-flex align-items-end flex-wrap">
                    <div class="mr-md-3 mr-xl-5">
                        <h2>{{ $title }}</h2>
                        <div class="d-flex">
                            <i class="mdi mdi-home text-muted hover-cursor"></i>
                            <p class="text-muted mb-0 hover-cursor">&nbsp;/&nbsp;Inventory&nbsp;/&nbsp;</p>
                            <p class="text-primary mb-0 hover-cursor">Data Jenis Asset</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-6 grid-margin stretch-card">
            <div class="card">
                <div class="card-body">
                    <h4 class="card-title">Form Tambah Jenis Asset</h4>
                    <form class="forms-sample">
                        <div class="form-group">
                            <label for="kode_jenis" class="form-label">Kode</label>
                            <input type="text" id="kode_jenis" class="form-control" placeholder="" disabled>
                        </div>
                        <div class="form-group">
                            <label for="nama_jenis">Nama Barang</label>
                            <input type="text" class="form-control" id="nama_jenis" placeholder="">
                        </div>
                        <div class="form-group">
                            <label for="nama_jenis">Nama Peminjam</label>
                            <input type="text" class="form-control" id="nama_jenis" placeholder="">
                        </div>
                        <div class="form-group">
                            <label for="nama_jenis">Tanggal Peminjaman</label>
                            <input type="date" class="form-control" id="nama_jenis" placeholder="">
                        </div>
                        <div class="form-group">
                            <label for="nama_jenis">Tanggal Pengembalian</label>
                            <input type="date" class="form-control" id="nama_jenis" placeholder="">
                        </div>
                        <div class="form-group">
                            <label for="nama_jenis">Lama Peminjaman</label>
                            <input type="text" class="form-control" id="nama_jenis" placeholder="">
                        </div>
                        <div class="form-group">
                            <label for="nama_jenis">Kondisi</label>
                            <input type="text" class="form-control" id="nama_jenis" placeholder="">
                        </div>
                        <div class="form-group">
                            <label for="nama_jenis">Denda</label>
                            <input type="text" class="form-control" id="nama_jenis" placeholder="">
                        </div>
                        <button type="submit" class="btn btn-primary mr-2">Submit</button>
                        <a href="/peminjaman/data_barang_peminjaman/create" class="btn btn-danger">Cancel</a>
                    </form>
                </div>
            </div>
        </div>

    </div>
@endsection
