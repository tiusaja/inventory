@extends('layouts.template')

@section('content')
    <div class="row">
        <div class="col-md-12 grid-margin">
            <div class="d-flex justify-content-between flex-wrap">
                <div class="d-flex align-items-end flex-wrap">
                    <div class="mr-md-3 mr-xl-5">
                        <h2>Data Barang Dipinjamkan</h2>
                        <div class="d-flex">
                            <i class="mdi mdi-home text-muted hover-cursor"></i>
                            <p class="text-muted mb-0 hover-cursor">&nbsp;/&nbsp;Inventory&nbsp;/&nbsp;</p>
                            <p class="text-primary mb-0 hover-cursor">Data Barang Dipinjamkan</p>
                        </div>
                    </div>
                </div>
                <div class="d-flex justify-content-between align-items-end flex-wrap">
                    <a href="/peminjaman/data_barang_peminjaman/create" class="btn btn-primary mt-2 mt-xl-0">Buat Data</a>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12 grid-margin stretch-card">
            <div class="card">
                <div class="card-body">
                    <h4 class="card-title">Data Barang Dipinjamkan</h4>
                    <div class="table-responsive">
                        <table class="table table-hover table-bordered" id="datatables">
                            <thead>
                                <tr class="bg-dark text-light">
                                    <th class="">No</th>
                                    <th class="col-2">Kode</th>
                                    <th class="col-2">Nama Barang</th>
                                    <th class="col-2">Nama Peminjam</th>
                                    <th class="col-1 text-center">Tanggal Peminjaman</th>
                                    <th class="col-1 text-center">Tanggal Pengembalian</th>
                                    <th class="col-1 text-center">Lama Peminjaman</th>
                                    <th class="col-2">Kondisi</th>
                                    <th class="col-2">Denda</th>
                                    <th class="col-1">Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>1</td>
                                    <td>69696969</td>
                                    <td>Photoshop</td>
                                    <td>Agus</td>
                                    <td>29/10/2021</td>
                                    <td>29/10/2022</td>
                                    <td>1 Tahun</td>
                                    <td>Baik</td>
                                    <td>-</td>
                                    <td class="text-center">
                                        <a href="/peminjaman/data_barang_peminjaman/edit">
                                            <i class="fas fa-edit float-left"></i>
                                        </a>
                                        <a href="">
                                            <i class="fas fa-trash-alt text-danger float-right"></i>
                                        </a>
                                    </td>
                                </tr>
                                <tr>
                                    <td>1</td>
                                    <td>69696969</td>
                                    <td>Photoshop</td>
                                    <td>Agus</td>
                                    <td>29/10/2021</td>
                                    <td>29/10/2022</td>
                                    <td>1 Tahun</td>
                                    <td>Baik</td>
                                    <td>-</td>
                                    <td class="text-center">
                                        <a href="#">
                                            <i class="fas fa-edit float-left"></i>
                                        </a>
                                        <a href="">
                                            <i class="fas fa-trash-alt text-danger float-right"></i>
                                        </a>
                                    </td>
                                </tr>
                                <tr>
                                    <td>1</td>
                                    <td>69696969</td>
                                    <td>Photoshop</td>
                                    <td>Agus</td>
                                    <td>29/10/2021</td>
                                    <td>29/10/2022</td>
                                    <td>1 Tahun</td>
                                    <td>Baik</td>
                                    <td>-</td>
                                    <td class="text-center">
                                        <a href="#">
                                            <i class="fas fa-edit float-left"></i>
                                        </a>
                                        <a href="">
                                            <i class="fas fa-trash-alt text-danger float-right"></i>
                                        </a>
                                    </td>
                                </tr>
                                <tr>
                                    <td>1</td>
                                    <td>69696969</td>
                                    <td>Photoshop</td>
                                    <td>Agus</td>
                                    <td>29/10/2021</td>
                                    <td>29/10/2022</td>
                                    <td>1 Tahun</td>
                                    <td>Baik</td>
                                    <td>-</td>
                                    <td class="text-center">
                                        <a href="#">
                                            <i class="fas fa-edit float-left"></i>
                                        </a>
                                        <a href="">
                                            <i class="fas fa-trash-alt text-danger float-right"></i>
                                        </a>
                                    </td>
                                </tr>
                                <tr>
                                    <td>1</td>
                                    <td>69696969</td>
                                    <td>Photoshop</td>
                                    <td>Agus</td>
                                    <td>29/10/2021</td>
                                    <td>29/10/2022</td>
                                    <td>1 Tahun</td>
                                    <td>Baik</td>
                                    <td>-</td>
                                    <td class="text-center">
                                        <a href="#">
                                            <i class="fas fa-edit float-left"></i>
                                        </a>
                                        <a href="">
                                            <i class="fas fa-trash-alt text-danger float-right"></i>
                                        </a>
                                    </td>
                                </tr>
                                <tr>
                                    <td>1</td>
                                    <td>69696969</td>
                                    <td>Photoshop</td>
                                    <td>Agus</td>
                                    <td>29/10/2021</td>
                                    <td>29/10/2022</td>
                                    <td>1 Tahun</td>
                                    <td>Baik</td>
                                    <td>-</td>
                                    <td class="text-center">
                                        <a href="#">
                                            <i class="fas fa-edit float-left"></i>
                                        </a>
                                        <a href="">
                                            <i class="fas fa-trash-alt text-danger float-right"></i>
                                        </a>
                                    </td>
                                </tr>
                                <tr>
                                    <td>1</td>
                                    <td>69696969</td>
                                    <td>Photoshop</td>
                                    <td>Agus</td>
                                    <td>29/10/2021</td>
                                    <td>29/10/2022</td>
                                    <td>1 Tahun</td>
                                    <td>Baik</td>
                                    <td>-</td>
                                    <td class="text-center">
                                        <a href="#">
                                            <i class="fas fa-edit float-left"></i>
                                        </a>
                                        <a href="">
                                            <i class="fas fa-trash-alt text-danger float-right"></i>
                                        </a>
                                    </td>
                                </tr>
                                <tr>
                                    <td>1</td>
                                    <td>69696969</td>
                                    <td>Photoshop</td>
                                    <td>Agus</td>
                                    <td>29/10/2021</td>
                                    <td>29/10/2022</td>
                                    <td>1 Tahun</td>
                                    <td>Baik</td>
                                    <td>-</td>
                                    <td class="text-center">
                                        <a href="#">
                                            <i class="fas fa-edit float-left"></i>
                                        </a>
                                        <a href="">
                                            <i class="fas fa-trash-alt text-danger float-right"></i>
                                        </a>
                                    </td>
                                </tr>
                                <tr>
                                    <td>1</td>
                                    <td>69696969</td>
                                    <td>Photoshop</td>
                                    <td>Agus</td>
                                    <td>29/10/2021</td>
                                    <td>29/10/2022</td>
                                    <td>1 Tahun</td>
                                    <td>Baik</td>
                                    <td>-</td>
                                    <td class="text-center">
                                        <a href="#">
                                            <i class="fas fa-edit float-left"></i>
                                        </a>
                                        <a href="">
                                            <i class="fas fa-trash-alt text-danger float-right"></i>
                                        </a>
                                    </td>
                                </tr>
                                <tr>
                                    <td>1</td>
                                    <td>69696969</td>
                                    <td>Photoshop</td>
                                    <td>Agus</td>
                                    <td>29/10/2021</td>
                                    <td>29/10/2022</td>
                                    <td>1 Tahun</td>
                                    <td>Baik</td>
                                    <td>-</td>
                                    <td class="text-center">
                                        <a href="#">
                                            <i class="fas fa-edit float-left"></i>
                                        </a>
                                        <a href="">
                                            <i class="fas fa-trash-alt text-danger float-right"></i>
                                        </a>
                                    </td>
                                </tr>
                                <tr>
                                    <td>1</td>
                                    <td>69696969</td>
                                    <td>Photoshop</td>
                                    <td>Agus</td>
                                    <td>29/10/2021</td>
                                    <td>29/10/2022</td>
                                    <td>1 Tahun</td>
                                    <td>Baik</td>
                                    <td>-</td>
                                    <td class="text-center">
                                        <a href="#">
                                            <i class="fas fa-edit float-left"></i>
                                        </a>
                                        <a href="">
                                            <i class="fas fa-trash-alt text-danger float-right"></i>
                                        </a>
                                    </td>
                                </tr>
                                <tr>
                                    <td>1</td>
                                    <td>69696969</td>
                                    <td>Photoshop</td>
                                    <td>Agus</td>
                                    <td>29/10/2021</td>
                                    <td>29/10/2022</td>
                                    <td>1 Tahun</td>
                                    <td>Baik</td>
                                    <td>-</td>
                                    <td class="text-center">
                                        <a href="#">
                                            <i class="fas fa-edit float-left"></i>
                                        </a>
                                        <a href="">
                                            <i class="fas fa-trash-alt text-danger float-right"></i>
                                        </a>
                                    </td>
                                </tr>
                                <tr>
                                    <td>1</td>
                                    <td>69696969</td>
                                    <td>Photoshop</td>
                                    <td>Agus</td>
                                    <td>29/10/2021</td>
                                    <td>29/10/2022</td>
                                    <td>1 Tahun</td>
                                    <td>Baik</td>
                                    <td>-</td>
                                    <td class="text-center">
                                        <a href="#">
                                            <i class="fas fa-edit float-left"></i>
                                        </a>
                                        <a href="">
                                            <i class="fas fa-trash-alt text-danger float-right"></i>
                                        </a>
                                    </td>
                                </tr>
                                <tr>
                                    <td>1</td>
                                    <td>69696969</td>
                                    <td>Photoshop</td>
                                    <td>Agus</td>
                                    <td>29/10/2021</td>
                                    <td>29/10/2022</td>
                                    <td>1 Tahun</td>
                                    <td>Baik</td>
                                    <td>-</td>
                                    <td class="text-center">
                                        <a href="#">
                                            <i class="fas fa-edit float-left"></i>
                                        </a>
                                        <a href="">
                                            <i class="fas fa-trash-alt text-danger float-right"></i>
                                        </a>
                                    </td>
                                </tr>
                                <tr>
                                    <td>1</td>
                                    <td>69696969</td>
                                    <td>Photoshop</td>
                                    <td>Agus</td>
                                    <td>29/10/2021</td>
                                    <td>29/10/2022</td>
                                    <td>1 Tahun</td>
                                    <td>Baik</td>
                                    <td>-</td>
                                    <td class="text-center">
                                        <a href="#">
                                            <i class="fas fa-edit float-left"></i>
                                        </a>
                                        <a href="">
                                            <i class="fas fa-trash-alt text-danger float-right"></i>
                                        </a>
                                    </td>
                                </tr>
                                <tr>
                                    <td>1</td>
                                    <td>69696969</td>
                                    <td>Photoshop</td>
                                    <td>Agus</td>
                                    <td>29/10/2021</td>
                                    <td>29/10/2022</td>
                                    <td>1 Tahun</td>
                                    <td>Baik</td>
                                    <td>-</td>
                                    <td class="text-center">
                                        <a href="#">
                                            <i class="fas fa-edit float-left"></i>
                                        </a>
                                        <a href="">
                                            <i class="fas fa-trash-alt text-danger float-right"></i>
                                        </a>
                                    </td>
                                </tr>
                                <tr>
                                    <td>1</td>
                                    <td>69696969</td>
                                    <td>Photoshop</td>
                                    <td>Agus</td>
                                    <td>29/10/2021</td>
                                    <td>29/10/2022</td>
                                    <td>1 Tahun</td>
                                    <td>Baik</td>
                                    <td>-</td>
                                    <td class="text-center">
                                        <a href="#">
                                            <i class="fas fa-edit float-left"></i>
                                        </a>
                                        <a href="">
                                            <i class="fas fa-trash-alt text-danger float-right"></i>
                                        </a>
                                    </td>
                                </tr>
                                <tr>
                                    <td>1</td>
                                    <td>69696969</td>
                                    <td>Photoshop</td>
                                    <td>Agus</td>
                                    <td>29/10/2021</td>
                                    <td>29/10/2022</td>
                                    <td>1 Tahun</td>
                                    <td>Baik</td>
                                    <td>-</td>
                                    <td class="text-center">
                                        <a href="#">
                                            <i class="fas fa-edit float-left"></i>
                                        </a>
                                        <a href="">
                                            <i class="fas fa-trash-alt text-danger float-right"></i>
                                        </a>
                                    </td>
                                </tr>
                                <tr>
                                    <td>1</td>
                                    <td>69696969</td>
                                    <td>Photoshop</td>
                                    <td>Agus</td>
                                    <td>29/10/2021</td>
                                    <td>29/10/2022</td>
                                    <td>1 Tahun</td>
                                    <td>Baik</td>
                                    <td>-</td>
                                    <td class="text-center">
                                        <a href="#">
                                            <i class="fas fa-edit float-left"></i>
                                        </a>
                                        <a href="">
                                            <i class="fas fa-trash-alt text-danger float-right"></i>
                                        </a>
                                    </td>
                                </tr>
                                <tr>
                                    <td>1</td>
                                    <td>69696969</td>
                                    <td>Photoshop</td>
                                    <td>Agus</td>
                                    <td>29/10/2021</td>
                                    <td>29/10/2022</td>
                                    <td>1 Tahun</td>
                                    <td>Baik</td>
                                    <td>-</td>
                                    <td class="text-center">
                                        <a href="#">
                                            <i class="fas fa-edit float-left"></i>
                                        </a>
                                        <a href="">
                                            <i class="fas fa-trash-alt text-danger float-right"></i>
                                        </a>
                                    </td>
                                </tr>
                                <tr>
                                    <td>1</td>
                                    <td>69696969</td>
                                    <td>Photoshop</td>
                                    <td>Agus</td>
                                    <td>29/10/2021</td>
                                    <td>29/10/2022</td>
                                    <td>1 Tahun</td>
                                    <td>Baik</td>
                                    <td>-</td>
                                    <td class="text-center">
                                        <a href="#">
                                            <i class="fas fa-edit float-left"></i>
                                        </a>
                                        <a href="">
                                            <i class="fas fa-trash-alt text-danger float-right"></i>
                                        </a>
                                    </td>
                                </tr>
                                <tr>
                                    <td>1</td>
                                    <td>69696969</td>
                                    <td>Photoshop</td>
                                    <td>Agus</td>
                                    <td>29/10/2021</td>
                                    <td>29/10/2022</td>
                                    <td>1 Tahun</td>
                                    <td>Baik</td>
                                    <td>-</td>
                                    <td class="text-center">
                                        <a href="#">
                                            <i class="fas fa-edit float-left"></i>
                                        </a>
                                        <a href="">
                                            <i class="fas fa-trash-alt text-danger float-right"></i>
                                        </a>
                                    </td>
                                </tr>
                                <tr>
                                    <td>1</td>
                                    <td>69696969</td>
                                    <td>Photoshop</td>
                                    <td>Agus</td>
                                    <td>29/10/2021</td>
                                    <td>29/10/2022</td>
                                    <td>1 Tahun</td>
                                    <td>Baik</td>
                                    <td>-</td>
                                    <td class="text-center">
                                        <a href="#">
                                            <i class="fas fa-edit float-left"></i>
                                        </a>
                                        <a href="">
                                            <i class="fas fa-trash-alt text-danger float-right"></i>
                                        </a>
                                    </td>
                                </tr>
                                <tr>
                                    <td>1</td>
                                    <td>69696969</td>
                                    <td>Photoshop</td>
                                    <td>Agus</td>
                                    <td>29/10/2021</td>
                                    <td>29/10/2022</td>
                                    <td>1 Tahun</td>
                                    <td>Baik</td>
                                    <td>-</td>
                                    <td class="text-center">
                                        <a href="#">
                                            <i class="fas fa-edit float-left"></i>
                                        </a>
                                        <a href="">
                                            <i class="fas fa-trash-alt text-danger float-right"></i>
                                        </a>
                                    </td>
                                </tr>
                                <tr>
                                    <td>1</td>
                                    <td>69696969</td>
                                    <td>Photoshop</td>
                                    <td>Agus</td>
                                    <td>29/10/2021</td>
                                    <td>29/10/2022</td>
                                    <td>1 Tahun</td>
                                    <td>Baik</td>
                                    <td>-</td>
                                    <td class="text-center">
                                        <a href="#">
                                            <i class="fas fa-edit float-left"></i>
                                        </a>
                                        <a href="">
                                            <i class="fas fa-trash-alt text-danger float-right"></i>
                                        </a>
                                    </td>
                                </tr>
                                <tr>
                                    <td>1</td>
                                    <td>69696969</td>
                                    <td>Photoshop</td>
                                    <td>Agus</td>
                                    <td>29/10/2021</td>
                                    <td>29/10/2022</td>
                                    <td>1 Tahun</td>
                                    <td>Baik</td>
                                    <td>-</td>
                                    <td class="text-center">
                                        <a href="#">
                                            <i class="fas fa-edit float-left"></i>
                                        </a>
                                        <a href="">
                                            <i class="fas fa-trash-alt text-danger float-right"></i>
                                        </a>
                                    </td>
                                </tr>
                                <tr>
                                    <td>1</td>
                                    <td>69696969</td>
                                    <td>Photoshop</td>
                                    <td>Agus</td>
                                    <td>29/10/2021</td>
                                    <td>29/10/2022</td>
                                    <td>1 Tahun</td>
                                    <td>Baik</td>
                                    <td>-</td>
                                    <td class="text-center">
                                        <a href="#">
                                            <i class="fas fa-edit float-left"></i>
                                        </a>
                                        <a href="">
                                            <i class="fas fa-trash-alt text-danger float-right"></i>
                                        </a>
                                    </td>
                                </tr>
                                <tr>
                                    <td>1</td>
                                    <td>69696969</td>
                                    <td>Photoshop</td>
                                    <td>Agus</td>
                                    <td>29/10/2021</td>
                                    <td>29/10/2022</td>
                                    <td>1 Tahun</td>
                                    <td>Baik</td>
                                    <td>-</td>
                                    <td class="text-center">
                                        <a href="#">
                                            <i class="fas fa-edit float-left"></i>
                                        </a>
                                        <a href="">
                                            <i class="fas fa-trash-alt text-danger float-right"></i>
                                        </a>
                                    </td>
                                </tr>
                                <tr>
                                    <td>1</td>
                                    <td>69696969</td>
                                    <td>Photoshop</td>
                                    <td>Agus</td>
                                    <td>29/10/2021</td>
                                    <td>29/10/2022</td>
                                    <td>1 Tahun</td>
                                    <td>Baik</td>
                                    <td>-</td>
                                    <td class="text-center">
                                        <a href="#">
                                            <i class="fas fa-edit float-left"></i>
                                        </a>
                                        <a href="">
                                            <i class="fas fa-trash-alt text-danger float-right"></i>
                                        </a>
                                    </td>
                                </tr>
                                <tr>
                                    <td>1</td>
                                    <td>69696969</td>
                                    <td>Photoshop</td>
                                    <td>Agus</td>
                                    <td>29/10/2021</td>
                                    <td>29/10/2022</td>
                                    <td>1 Tahun</td>
                                    <td>Baik</td>
                                    <td>-</td>
                                    <td class="text-center">
                                        <a href="#">
                                            <i class="fas fa-edit float-left"></i>
                                        </a>
                                        <a href="">
                                            <i class="fas fa-trash-alt text-danger float-right"></i>
                                        </a>
                                    </td>
                                </tr>
                                <tr>
                                    <td>1</td>
                                    <td>69696969</td>
                                    <td>Photoshop</td>
                                    <td>Agus</td>
                                    <td>29/10/2021</td>
                                    <td>29/10/2022</td>
                                    <td>1 Tahun</td>
                                    <td>Baik</td>
                                    <td>-</td>
                                    <td class="text-center">
                                        <a href="#">
                                            <i class="fas fa-edit float-left"></i>
                                        </a>
                                        <a href="">
                                            <i class="fas fa-trash-alt text-danger float-right"></i>
                                        </a>
                                    </td>
                                </tr>
                                <tr>
                                    <td>1</td>
                                    <td>69696969</td>
                                    <td>Photoshop</td>
                                    <td>Agus</td>
                                    <td>29/10/2021</td>
                                    <td>29/10/2022</td>
                                    <td>1 Tahun</td>
                                    <td>Baik</td>
                                    <td>-</td>
                                    <td class="text-center">
                                        <a href="#">
                                            <i class="fas fa-edit float-left"></i>
                                        </a>
                                        <a href="">
                                            <i class="fas fa-trash-alt text-danger float-right"></i>
                                        </a>
                                    </td>
                                </tr>
                                <tr>
                                    <td>1</td>
                                    <td>69696969</td>
                                    <td>Photoshop</td>
                                    <td>Agus</td>
                                    <td>29/10/2021</td>
                                    <td>29/10/2022</td>
                                    <td>1 Tahun</td>
                                    <td>Baik</td>
                                    <td>-</td>
                                    <td class="text-center">
                                        <a href="#">
                                            <i class="fas fa-edit float-left"></i>
                                        </a>
                                        <a href="">
                                            <i class="fas fa-trash-alt text-danger float-right"></i>
                                        </a>
                                    </td>
                                </tr>
                                <tr>
                                    <td>1</td>
                                    <td>69696969</td>
                                    <td>Photoshop</td>
                                    <td>Agus</td>
                                    <td>29/10/2021</td>
                                    <td>29/10/2022</td>
                                    <td>1 Tahun</td>
                                    <td>Baik</td>
                                    <td>-</td>
                                    <td class="text-center">
                                        <a href="#">
                                            <i class="fas fa-edit float-left"></i>
                                        </a>
                                        <a href="">
                                            <i class="fas fa-trash-alt text-danger float-right"></i>
                                        </a>
                                    </td>
                                </tr>
                                <tr>
                                    <td>1</td>
                                    <td>69696969</td>
                                    <td>Photoshop</td>
                                    <td>Agus</td>
                                    <td>29/10/2021</td>
                                    <td>29/10/2022</td>
                                    <td>1 Tahun</td>
                                    <td>Baik</td>
                                    <td>-</td>
                                    <td class="text-center">
                                        <a href="#">
                                            <i class="fas fa-edit float-left"></i>
                                        </a>
                                        <a href="">
                                            <i class="fas fa-trash-alt text-danger float-right"></i>
                                        </a>
                                    </td>
                                </tr>
                                <tr>
                                    <td>1</td>
                                    <td>69696969</td>
                                    <td>Photoshop</td>
                                    <td>Agus</td>
                                    <td>29/10/2021</td>
                                    <td>29/10/2022</td>
                                    <td>1 Tahun</td>
                                    <td>Baik</td>
                                    <td>-</td>
                                    <td class="text-center">
                                        <a href="#">
                                            <i class="fas fa-edit float-left"></i>
                                        </a>
                                        <a href="">
                                            <i class="fas fa-trash-alt text-danger float-right"></i>
                                        </a>
                                    </td>
                                </tr>
                                <tr>
                                    <td>1</td>
                                    <td>69696969</td>
                                    <td>Photoshop</td>
                                    <td>Agus</td>
                                    <td>29/10/2021</td>
                                    <td>29/10/2022</td>
                                    <td>1 Tahun</td>
                                    <td>Baik</td>
                                    <td>-</td>
                                    <td class="text-center">
                                        <a href="#">
                                            <i class="fas fa-edit float-left"></i>
                                        </a>
                                        <a href="">
                                            <i class="fas fa-trash-alt text-danger float-right"></i>
                                        </a>
                                    </td>
                                </tr>
                                <tr>
                                    <td>1</td>
                                    <td>69696969</td>
                                    <td>Photoshop</td>
                                    <td>Agus</td>
                                    <td>29/10/2021</td>
                                    <td>29/10/2022</td>
                                    <td>1 Tahun</td>
                                    <td>Baik</td>
                                    <td>-</td>
                                    <td class="text-center">
                                        <a href="#">
                                            <i class="fas fa-edit float-left"></i>
                                        </a>
                                        <a href="">
                                            <i class="fas fa-trash-alt text-danger float-right"></i>
                                        </a>
                                    </td>
                                </tr>
                                <tr>
                                    <td>1</td>
                                    <td>69696969</td>
                                    <td>Photoshop</td>
                                    <td>Agus</td>
                                    <td>29/10/2021</td>
                                    <td>29/10/2022</td>
                                    <td>1 Tahun</td>
                                    <td>Baik</td>
                                    <td>-</td>
                                    <td class="text-center">
                                        <a href="#">
                                            <i class="fas fa-edit float-left"></i>
                                        </a>
                                        <a href="">
                                            <i class="fas fa-trash-alt text-danger float-right"></i>
                                        </a>
                                    </td>
                                </tr>
                                <tr>
                                    <td>1</td>
                                    <td>69696969</td>
                                    <td>Meja</td>
                                    <td>Agus</td>
                                    <td>29/10/2021</td>
                                    <td>29/10/2022</td>
                                    <td>1 Tahun</td>
                                    <td>Baik</td>
                                    <td>-</td>
                                    <td class="text-center">
                                        <a href="#">
                                            <i class="fas fa-edit float-left"></i>
                                        </a>
                                        <a href="">
                                            <i class="fas fa-trash-alt text-danger float-right"></i>
                                        </a>
                                    </td>
                                </tr>
                                <tr>
                                    <td>1</td>
                                    <td>69696969</td>
                                    <td>Photoshop</td>
                                    <td>Syurya</td>
                                    <td>29/10/2021</td>
                                    <td>29/10/2022</td>
                                    <td>1 Tahun</td>
                                    <td>Baik</td>
                                    <td>-</td>
                                    <td class="text-center">
                                        <a href="#">
                                            <i class="fas fa-edit float-left"></i>
                                        </a>
                                        <a href="">
                                            <i class="fas fa-trash-alt text-danger float-right"></i>
                                        </a>
                                    </td>
                                </tr>

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>


    </div>
@endsection

@section('costume_js')
    <script src="assets/vendors/datatables.net/jquery.dataTables.js"></script>
    <script src="assets/vendors/datatables.net-bs4/dataTables.bootstrap4.js"></script>
    <script>
        $('#datatables').DataTable({
            scrollY: 500,
            scrollX: true,
            scrollcollapse: true,
            paging: false
        });
    </script>
@endsection
