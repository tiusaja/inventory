@extends('layouts.template')

@section('content')
<div class="row">
    <div class="col-md-12 grid-margin">
        <div class="d-flex justify-content-between flex-wrap">
            <div class="d-flex align-items-end flex-wrap">
                <div class="mr-md-3 mr-xl-5">
                    <h2>Daftar Surat</h2>
                    <div class="d-flex">
                        <i class="mdi mdi-home text-muted hover-cursor"></i>
                        <p class="text-muted mb-0 hover-cursor">&nbsp;/&nbsp;Surat Masuk&nbsp;/&nbsp;</p>
                        <p class="text-primary mb-0 hover-cursor">Daftar Surat</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-lg-12 grid-margin stretch-card">
        <div class="card">
            <div class="card-body">
                <h4 class="card-title">Daftar Surat</h4>
                <div class="table-responsive">
                    <table class="table table-hover table-bordered" id="datatables">
                        <thead>
                            <tr class="bg-dark text-light">
                                <th class="">No</th>
                                <th class="col-2">Nomor Surat</th>
                                <th class="col-2">Tanggal</th>
                                <th class="col-2">Perihal</th>
                                <th class="col-1 text-center">Lampiran</th>
                                <th class="col-1 text-center">Ditujukan Kepada</th>
                                <th class="col-1 text-center">Acara</th>
                                <th class="col-2">Tujuan</th>
                                <th class="col-2">Tanggal Acara</th>
                                <th class="col-2">Waktu </th>
                                <th class="col-2">Gambar Surat </th>
                                <th class="col-2">View </th>
                                <th class="col-1">Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td><a href="/suratMasuk/undanganCetak" type="submit" class="btn btn-success mr-2">View</a></td>
                                <td class="text-center">
                                    <a href="">
                                        <i class="fas fa-edit float-left"></i>
                                    </a>
                                    <a href="">
                                        <i class="fas fa-trash-alt text-danger float-right"></i>
                                    </a>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>


</div>
@endsection

@section('costume_js')
<script src="assets/vendors/datatables.net/jquery.dataTables.js"></script>
<script src="assets/vendors/datatables.net-bs4/dataTables.bootstrap4.js"></script>
<script>
    $('#datatables').DataTable({
        scrollY: 500,
        scrollX: true,
        scrollcollapse: true,
        paging: false
    });
</script>
@endsection
