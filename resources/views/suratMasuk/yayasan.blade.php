@extends('layouts.template')
@section('costume_css')
<link rel="stylesheet" href="assets/vendors/datatables.net-bs4/dataTables.boostrap4.css">
@endsection
@section('content')
<div class="row">
    <div class="col-md-12 grid-margin">
        <div class="d-flex justify-content-between flex-wrap">
            <div class="d-flex align-items-end flex-wrap">
                <div class="mr-md-3 mr-xl-5">
                    <h2>{{ $title }}</h2>
                    <div class="d-flex">
                        <i class="mdi mdi-home text-muted hover-cursor"></i>
                        <p class="text-muted mb-0 hover-cursor">&nbsp;/&nbsp;Surat Masuk&nbsp;/&nbsp;</p>
                        <p class="text-primary mb-0 hover-cursor">Buat Surat</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-12 grid-margin stretch-card">
        <div class="card">
            <div class="card-body mb-3">
                <h2 class="card-title">Form Tambah Surat</h2>
                <a href="/suratMasuk/daftarDinas" type="submit" class="btn btn-outline-danger m-2">Daftar Surat Dinas</a>
                <a href="/suratMasuk/daftarUndangan" type="submit" class="btn btn-outline-danger m-2">Daftar Surat Undangan</a>
                <a href="/suratMasuk/daftarKeputusan" type="submit" class="btn btn-outline-danger m-2">Daftar Surat Keputusan</a>
                <a href="/suratMasuk/daftarTugas" type="submit" class="btn btn-outline-danger m-2">Daftar Surat Tugas</a>
                <a href="/suratMasuk/daftarPengumuman" type="submit" class="btn btn-outline-danger m-2">Daftar Surat Pengumuman</a>
                <a href="/suratMasuk/daftarYayasan" type="submit" class="btn btn-outline-danger m-2">Daftar Surat Yayasan</a>
                <a href="/suratMasuk/daftarMou" type="submit" class="btn btn-outline-danger m-2">Daftar Surat MoU</a>
                <form class="row g-2 mb-3 mt-5">
                    <div class="col-md-6 mb-4">
                            <label for="pilih_kop">Pilih Kop Surat</label>
                            <select class="form-control border-dark" id="pilih_kop">
                                <option>SMKN 1 Magelang</option>
                                <option>SMKN 2 Magelang</option>
                                <option>SMKN 3 Magelang</option>
                            </select>
                    </div>
                    <div class="col-md-6 mb-4">
                        <label for="nomor_surat" class="visually-hidden">Nomor Surat</label>
                        <input type="integer" class="form-control border border-dark" id="nomor_surat">
                    </div>
                    <div class="col-md-6 mb-4">
                        <label for="nama" class="visually-hidden">Nama</label>
                        <input type="text" class="form-control border-dark" id="text" placeholder="Nama">
                    </div>
                    <div class="col-md-3 mb-4">
                        <label for="ntempat" class="visually-hidden">Tempat</label>
                        <input type="text" class="form-control border border-dark" id="noSurat">
                    </div>
                    <div class="col-md-3 mb-4">
                        <label for="tgl" class="visually-hidden">Tanggal</label>
                        <input type="date" class="form-control border border-dark" id="noSurat">
                    </div>
                    <div class="col-md-6 mb-4">
                        <label for="nuptk" class="visually-hidden">NIP / NIS</label>
                        <input type="integer" class="form-control border-dark" id="NUPTK" placeholder="NUPTK">
                    </div>
                    <div class="col-md-6 mb-4">
                            <label for="jabatan">Jabatan</label>
                            <select class="form-control border-dark" id="jabatan">
                                <option>Guru</option>
                                <option>Staff</option>
                                <option>Siswa</option>
                            </select>
                    </div>
                    <div class="col-md-6 mb-4">
                        <label for="instansi" class="visually-hidden">Instansi</label>
                        <input type="text" class="form-control border-dark" id="instansi" placeholder="Unit Kerja">
                    </div>
                    <div class="col-md-6 mb-4">
                            <label for="image" class="visually-hidden">Masukkan Gambar Surat Fisik</label>
                            <input type="file" class="form-control border-dark" id="gambar_surat" placeholder="Lampiran Surat Fisik">
                    </div>
                    <div class="col-md-6 mb-4">
                        <label for="isi" class="visually-hidden">Isi Surat (Yayasan) </label>
                        <textarea class="form-control border-dark" placeholder="isi" id="isi" style="height: 150px"></textarea>
                    </div>
                    <div class="col-md-6 mb-4">

                    </div>
                    <div class="col-md-6 mb-4 mt-4">
                <a href="/suratMasuk/yayasanCetak" type="submit" class="btn btn-primary mr-2">Submit</a>
                <a href="" class="btn btn-danger">Cancel</a>
            </div>
            </div>
            </form>
        </div>
    </div>
</div>
</div>


</div>
@endsection
