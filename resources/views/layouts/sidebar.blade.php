<nav class="sidebar sidebar-offcanvas" id="sidebar">
    <ul class="nav">
        <li class="nav-item {{ $title === 'Home' ? 'active' : '' }}">
            <a class="nav-link" href="/dashboard/home">
                <i class="mdi mdi-home menu-icon"></i>
                <span class="menu-title">Dashboard</span>
            </a>
        </li>
        <li class="nav-item">
            <a class="nav-link" data-toggle="collapse" href="#ui-masuk" aria-expanded="false" aria-controls="ui-masuk">
                <i class="mdi mdi-book-open-variant menu-icon"></i>
                <span class="menu-title">Surat Masuk</span>
                <i class="menu-arrow"></i>
            </a>
            <div class="collapse" id="ui-masuk">
                <ul class="nav flex-column sub-menu">
                    <li class="nav-item {{ $title === 'Buat Surat' ? 'active' : '' }}"> <a class="nav-link" href="/suratMasuk/createSurat">Surat Dinas</a></li>
                    <li class="nav-item {{ $title === 'Buat Surat' ? 'active' : '' }}"> <a class="nav-link" href="/suratMasuk/undangan">Surat Undangan</a></li>
                    <li class="nav-item {{ $title === 'Buat Surat' ? 'active' : '' }}"> <a class="nav-link" href="/suratMasuk/keputusan">Surat Keputusan</a></li>
                    <li class="nav-item {{ $title === 'Buat Surat' ? 'active' : '' }}"> <a class="nav-link" href="/suratMasuk/tugas">Surat Tugas</a></li>
                    <li class="nav-item {{ $title === 'Buat Surat' ? 'active' : '' }}"> <a class="nav-link" href="/suratMasuk/pengumuman">Surat Pengumuman</a></li>
                    <li class="nav-item {{ $title === 'Buat Surat' ? 'active' : '' }}"> <a class="nav-link" href="/suratMasuk/yayasan">Surat Yayasan</a></li>
                    <li class="nav-item {{ $title === 'Buat Surat' ? 'active' : '' }}"> <a class="nav-link" href="/suratMasuk/mou">Surat MoU</a></li>
                </ul>
            </div>
        </li>
        <li class="nav-item">
            <a class="nav-link" data-toggle="collapse" href="#ui-keluar" aria-expanded="false" aria-controls="ui-keluar">
                <i class="mdi mdi-book-open-page-variant menu-icon"></i>
                <span class="menu-title">Surat Keluar</span>
                <i class="menu-arrow"></i>
            </a>
            <div class="collapse" id="ui-keluar">
                <ul class="nav flex-column sub-menu">
                    <li class="nav-item {{ $title === 'Buat Surat' ? 'active' : '' }}"> <a class="nav-link" href="/suratKeluar/createSurat">Surat Dinas</a></li>
                    <li class="nav-item {{ $title === 'Buat Surat' ? 'active' : '' }}"> <a class="nav-link" href="/suratKeluar/undangan">Surat Undangan</a></li>
                    <li class="nav-item {{ $title === 'Buat Surat' ? 'active' : '' }}"> <a class="nav-link" href="/suratKeluar/keputusan">Surat Keputusan</a></li>
                    <li class="nav-item {{ $title === 'Buat Surat' ? 'active' : '' }}"> <a class="nav-link" href="/suratKeluar/tugas">Surat Tugas</a></li>
                    <li class="nav-item {{ $title === 'Buat Surat' ? 'active' : '' }}"> <a class="nav-link" href="/suratKeluar/pengumuman">Surat Pengumuman</a></li>
                    <li class="nav-item {{ $title === 'Buat Surat' ? 'active' : '' }}"> <a class="nav-link" href="/suratKeluar/yayasan">Surat Yayasan</a></li>
                    <li class="nav-item {{ $title === 'Buat Surat' ? 'active' : '' }}"> <a class="nav-link" href="/suratKeluar/mou">Surat MoU</a></li>
                </ul>

            </div>
        </li>
        <li class="nav-item">
            <a class="nav-link" data-toggle="collapse" href="#ui-disposisi" aria-expanded="false" aria-controls="ui-disposisi">
                <i class="mdi mdi-account-check menu-icon"></i>
                <span class="menu-title">Disposisi</span>
                <i class="menu-arrow"></i>
            </a>
            <div class="collapse" id="ui-disposisi">
                <ul class="nav flex-column sub-menu">
                    <li class="nav-item {{ $title === 'Daftar Disposisi' ? 'active' : '' }}"> <a class="nav-link" href="/disposisi/daftarDisposisi">Daftar Disposisi</a></li>
                </ul>
            </div>
        </li>
        <li class="nav-item">
            <a class="nav-link" data-toggle="collapse" href="#ui-sekolah" aria-expanded="false" aria-controls="ui-sekolah">
                <i class="mdi mdi-school menu-icon"></i>
                <span class="menu-title">Sekolah</span>
                <i class="menu-arrow"></i>
            </a>
            <div class="collapse" id="ui-sekolah">
                <ul class="nav flex-column sub-menu">
                    <li class="nav-item {{ $title === 'Tambah Sekolah' ? 'active' : '' }}"> <a class="nav-link" href="/sekolah/createSekolah">Tambah Sekolah
                        </a></li>
                    <li class="nav-item {{ $title === 'Daftar Sekolah' ? 'active' : '' }}"> <a class="nav-link" href="/sekolah/daftarSekolah">Daftar Sekolah</a></li>
                </ul>
            </div>
        </li>
        <li class="nav-item">
            <a class="nav-link" data-toggle="collapse" href="#ui-user" aria-expanded="false" aria-controls="ui-user">
                <i class="mdi mdi-account-circle menu-icon"></i>
                <span class="menu-title">User</span>
                <i class="menu-arrow"></i>
            </a>
            <div class="collapse" id="ui-user">
                <ul class="nav flex-column sub-menu">
                    <li class="nav-item {{ $title === 'Tambah User' ? 'active' : '' }}"> <a class="nav-link" href="/user/createUser">Tambah User
                        </a></li>
                    <li class="nav-item {{ $title === 'Daftar User' ? 'active' : '' }}"> <a class="nav-link" href="/user/daftarUser">Daftar User</a></li>
                </ul>
            </div>
        </li>

        <li class="nav-item {{ $title === 'Home' ? 'active' : '' }}">
            <a class="nav-link" href="/dashboard/home">
                <i class="mdi mdi-application menu-icon"></i>
                <span class="menu-title">BackUp Database</span>
            </a>
        </li>
    </ul>
</nav>
