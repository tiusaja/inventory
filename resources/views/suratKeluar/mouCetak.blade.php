<!DOCTYPE html>
<html>
<head>
	<title>Surat Tugas</title>
	<style type="text/css">
		table {
			border-style: double;
			border-width: 3px;
			border-color: white;
		}
		table tr .text2 {
			text-align: right;
			font-size: 13px;
		}
		table tr .text {
			text-align: center;
			font-size: 13px;
		}
		table tr td {
			font-size: 13px;
		}

	</style>
</head>
<body>
	<center>
		<table>
			<tr>
				<td><img src="public/assets/images/LOGO.jpeg" width="90" height="90"></td>
				<td>
				<center>
					<font size="4">PEMERINTAH KOTA MAGELANG</font><br>
                    <font size="4">DINAS PENDIDIKAN KOTA MAGELANG</font><br>
					<font size="5"><b>SEKOLAH MENENGAH KEJURUAN NEGERI 2 MAGELANG</b></font><br>
					<font size="2">Bidang Keahlian : Akutansi - Perkantoran - Rekayasa Perangkat Lunak - Pemasaran</font><br>
					<font size="2"><i>135, Jl.Ahmad Yani,Keramat Selatan Kode Pos : 59155 Telp./Fax (0293)362577 Kec.Magelang Utara, Kota Magelang</i></font>
				</center>
				</td>
			</tr>
			<tr>
				<td colspan="2"><hr></td>
			</tr>
		<table width="625">
			<tr>
            <font size="5"><b>SURAT MOU</b></font><br>
            <font size="3"><b>Nomor Surat <td></td></b><font><br>
                				
			</tr>
		</table>
		</table>
		
		<table>
       <tr class="text2">
				<td>Nama Pihak Pertama</td>
				<td width="541">: <b></b></td>
                <td></td>
			</tr>
			<tr>
				<td>Jabatan Pihak Pertama</td>
				<td width="525">: </td>
                <td></td>
			</tr>
			<tr>
				<td>Alamat Pihak Pertama</td>
				<td width="525">:</td>
                <td></td>
			</tr>
            <tr>
				<td>Instansi Pihak Pertama</td>
				<td width="525">:</td>
                <td></td>
			</tr>
            <tr>
				<td>Nomor Pihak Pertama</td>
				<td width="525">:</td>
                <td></td>
			</tr>
    </table>
		<br>
		<table width="625">
			<tr>
		       <td>
			       <font size="2">Selanjutnya disebut pihak pertama<td></td></font>
		       </td>
		    </tr>
		</table>
		<br>
        <table>
       <tr class="text2">
				<td>Nama Pihak Kedua</td>
				<td width="541">: <b></b></td>
                <td></td>
			</tr>
			<tr>
				<td>Jabatan Pihak Kedua</td>
				<td width="525">: </td>
                <td></td>
			</tr>
			<tr>
				<td>Alamat Pihak Kedua</td>
				<td width="525">:</td>
                <td></td>
			</tr>
            <tr>
				<td>Instansi Pihak Kedua</td>
				<td width="525">:</td>
                <td></td>
			</tr>
            <tr>
				<td>Nomor Pihak Kedua</td>
				<td width="525">:</td>
                <td></td>
			</tr>
            <br>
		<table width="625">
			<tr>
		       <td>
			       <font size="2">Selanjutnya disebut pihak kedua<td></td></font>
		       </td>
		    </tr>
		</table>
		<br>
    </table>
    <table width="680">
			<tr>
		       <td>
			       <font size="2">Dengan surat ini kedua belah pihak telah menyepakati untuk melakukan kerjasama<td></td></font>
		       </td>
		    </tr>
		</table>
		<table width="400">
			<tr>
				<td width="100"><br><br><br><br></td>
				<td class="text" align="center">Selaku Pihak Kedua<br><br><br><br>Nama<td></td></td>
			</tr>
	     </table>
         <table width="700">
			<tr>
				<td width="400"><br><br><br><br></td>
				<td class="text" align="center">Kepala Sekolah<br><br><br><br>Nama<td></td></td>
			</tr>
	     </table>
	</center>
</body>
</html>