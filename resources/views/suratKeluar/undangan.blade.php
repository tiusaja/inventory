@extends('layouts.template')
@section('costume_css')
<link rel="stylesheet" href="assets/vendors/datatables.net-bs4/dataTables.boostrap4.css">
@endsection
@section('content')
<div class="row">
    <div class="col-md-12 grid-margin">
        <div class="d-flex justify-content-between flex-wrap">
            <div class="d-flex align-items-end flex-wrap">
                <div class="mr-md-3 mr-xl-5">
                    <h2>{{ $title }}</h2>
                    <div class="d-flex">
                        <i class="mdi mdi-home text-muted hover-cursor"></i>
                        <p class="text-muted mb-0 hover-cursor">&nbsp;/&nbsp;Surat Keluar&nbsp;/&nbsp;</p>
                        <p class="text-primary mb-0 hover-cursor">Buat Surat</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-12 grid-margin stretch-card">
        <div class="card">
            <div class="card-body mb-3">
                <h2 class="card-title">Form Tambah Surat</h2>
                <a href="/suratKeluar/daftarDinas" type="submit" class="btn btn-outline-danger m-2">Daftar Surat Dinas</a>
                <a href="/suratKeluar/daftarUndangan" type="submit" class="btn btn-outline-danger m-2">Daftar Surat Undangan</a>
                <a href="/suratKeluar/daftarKeputusan" type="submit" class="btn btn-outline-danger m-2">Daftar Surat Keputusan</a>
                <a href="/suratKeluar/daftarTugas" type="submit" class="btn btn-outline-danger m-2">Daftar Surat Tugas</a>
                <a href="/suratKeluar/daftarPengumuman" type="submit" class="btn btn-outline-danger m-2">Daftar Surat Pengumuman</a>
                <a href="/suratKeluar/daftarYayasan" type="submit" class="btn btn-outline-danger m-2">Daftar Surat Yayasan</a>
                <a href="/suratKeluar/daftarMou" type="submit" class="btn btn-outline-danger m-2">Daftar Surat MoU</a>
                <form class="row g-2 mb-3 mt-5">
                    <div class="col-md-6 mb-4">
                            <label for="pilih_kop">Pilih Kop Surat</label>
                            <select class="form-control border-dark" id="pilih_kop">
                                <option>SMKN 1 Magelang</option>
                                <option>SMKN 2 Magelang</option>
                                <option>SMKN 3 Magelang</option>
                            </select>
                    </div>
                    <div class="col-md-6 mb-4">
                        <label for="noSurat" class="visually-hidden">Nomor Surat</label>
                        <input type="integer" class="form-control border border-dark" id="noSurat">
                    </div>
                    <div class="col-md-6 mb-4">
                        <label for="tanggal" class="visually-hidden">Tanggal</label>
                        <input type="date" class="form-control border-dark" id="tanggal" placeholder="Tanggal">
                    </div>
                    <div class="col-md-6 mb-4">
                        <label for="perihal" class="visually-hidden">Perihal</label>
                        <input type="text" class="form-control border-dark" id="perihal" placeholder="Perihal">
                    </div>
                    <div class="col-md-6 mb-4">
                        <label for="lampiran" class="visually-hidden">Lampiran</label>
                        <input type="text" class="form-control border-dark" id="lampiran" placeholder="Lampiran">
                    </div>
                    <div class="col-md-6 mb-4">
                            <label for="ditujukan">Ditujukan Kepada</label>
                            <select class="form-control border-dark" id="ditujukan">
                                <option>Kelas X</option>
                                <option>Kelas XI</option>
                                <option>Kelas XII</option>
                            </select>
                    </div>
                    <div class="col-md-6 mb-4">
                        <label for="acara" class="visually-hidden">Acara</label>
                        <input type="text" class="form-control border-dark" id="acara" placeholder="Acara">
                    </div>
                    <div class="col-md-6 mb-4">
                        <label for="tujuan" class="visually-hidden">Tujuan</label>
                        <input type="text" class="form-control border-dark" id="tujuan" placeholder="Tujuan">
                    </div>
                    <div class="col-md-6 mb-4">
                        <label for="tanggalAcara" class="visually-hidden">Tanggal Acara</label>
                        <input type="date" class="form-control border-dark" id="tanggalAcara" placeholder="Tanggal Acara">
                    </div>
                    <div class="col-md-6 mb-4">
                        <label for="waktu" class="visually-hidden">Waktu</label>
                        <input type="text" class="form-control border-dark" id="waktu" placeholder="Waktu">
                    </div>

                    <a href="/suratKeluar/undanganCetak" type="submit" class="btn btn-primary mr-2">Submit</a>
                    <a href="" class="btn btn-danger">Cancel</a>
                </form>
            </div>
        </div>
    </div>


</div>
@endsection
