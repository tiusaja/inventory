<!doctype html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">


    <style>
        hr {
            background-color: black;
            border-top: 5px solid black;
            color: black;
            height: 100px;
            width: 100%;
        }

        img {
            margin-right: 30px;
            width: 180px;
            margin-bottom: 30px;
            float: left;

        }
    </style>

    <title>Template Surat</title>
</head>

<body>

    <div class="container mt-5">
        <div class="row  justify-content-center">
            <div class="col-md-12">
                <div class="kop text-center">
                    <img src="{{ asset('assets/images/tutWuri.png') }}" />
                    <h3 id=judul>PEMERINTAH DAERAH KHUSUS KOTA MAGELANG DINAS PENDIDIKAN</h3>
                    <h4 style="font-weight : normal">SEKOLAH MENENGAH KEJURUAN NEGERI 2 KOTA MAGELANG </h4>
                    <h5 style="font-weight : normal">Jalan Ahmad Yani No.0 Kota Magelang </h5>
                    <hr>
                </div>

                <table class="table table-borderless">
                    <tr style>
                        <td style="width: 30%;">No</td>
                        <td style="width: 5%;">:</td>
                        <td style="width: 6%;"></td>
                    </tr>
                    <tr>
                        <td style="width: 30%;">Lamp</td>
                        <td style="width: 5%;">:</td>
                        <td style="width: 65%;"></td>
                    </tr>
                    <tr>
                        <td style="width: 30%; vertical-align: top;">Hal</td>
                        <td style="width: 5%; vertical-align: top;">:</td>
                        <td style="width: 65%;"></td>
                    </tr>
                </table>
                <h6> Kepada </h6>
                <h6> </h6>
                <h6> Di </h6>
                <h6> Tempat </h6></br>

                <p>Assalamualaikum Wr Wb</p>
                <p>Dengan ini, Kepala Sekolah SMKN 2 MAGELANG mengundang untuk menghadiri. Dengan Tujuan. Rapat ini akan diadakan pada: </p>
                <table class="table table-borderless">
                    <tr style>
                        <td style="width: 30%;">Hari/Tanggal</td>
                        <td style="width: 5%;">:</td>
                        <td style="width: 6%;"></td>
                    </tr>
                    <tr>
                        <td style="width: 30%;">Waktu</td>
                        <td style="width: 5%;">:</td>
                        <td style="width: 65%;"></td>
                    </tr>
                    <tr>
                        <td style="width: 30%; vertical-align: top;">Tempat</td>
                        <td style="width: 5%; vertical-align: top;">:</td>
                        <td style="width: 65%;"></td>
                    </tr>
                </table>
                <p>Dengan ini diharapkan kehadiran. Demikian surat pemberitahuan ini kami sampaikan, atas perhatian dan kehadirannya kami sampaikan terima kasih</p>
                <p>Waalaikumsalam Wr Wb</p></br>
                <div class="text-end">
                    <p>Kepala SMKN 2 Magelang</p></br>
                    <p>Mila Yustiana S.Pd, M.MPar</p>
                    <p>NIP : 10101010</p>
                </div>
            </div>
        </div>
    </div>


    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script>
</body>

</html>