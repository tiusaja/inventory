<!DOCTYPE html>
<html>
<head>
	<title>Surat Undangan</title>
	<style type="text/css">
		table {
			border-style: double;
			border-width: 3px;
			border-color: white;
		}
		table tr .text2 {
			text-align: right;
			font-size: 13px;
		}
		table tr .text {
			text-align: center;
			font-size: 13px;
		}
		table tr td {
			font-size: 13px;
		}

	</style>
</head>
<body>
	<center>
		<table>
			<tr>
				<td><img src="{{ asset('assets/images/LOGO.jpeg') }} "width="90" height="90">
                    </td>
				<td>
				<center>
					<font size="4">PEMERINTAH KOTA MAGELANG</font><br>
                    <font size="4">DINAS PENDIDIKAN KOTA MAGELANG</font><br>
					<font size="5"><b>SEKOLAH MENENGAH KEJURUAN NEGERI 2 MAGELANG</b></font><br>
					<font size="2">Bidang Keahlian : Akutansi - Perkantoran - Rekayasa Perangkat Lunak - Pemasaran</font><br>
					<font size="2"><i>135, Jl.Ahmad Yani,Keramat Selatan Kode Pos : 59155 Telp./Fax (0293)362577 Kec.Magelang Utara, Kota Magelang</i></font>
				</center>
				</td>
			</tr>
			<tr>
				<td colspan="2"><hr></td>
			</tr>
		<table width="625">
			<tr>
				<td class="text2">Tanggal Surat<td></td></td>

			</tr>
		</table>
		</table>
		<table>
			<tr class="text2">
				<td>Nomer</td>
				<td width="572">: -</td>
                <td></td>
			</tr>
            <tr>
				<td>Lampiran</td>
				<td width="564">: -</td>
                <td></td>
			</tr>
			<tr>
				<td>Perihal</td>
				<td width="564">: -</td>
                <td></td>
			</tr>
		</table>
		<br>
		<table width="625">
			<tr>
		       <td>
			       <font size="2">Kpd yth.<br><br>Di tempat</font>
		       </td>
		    </tr>
		</table>
		<br>
		<table width="625">
			<tr>
		       <td>
			       <font size="2">Assalamu'alaikum wr.wb<br>Dengan ini, Kepala Sekolah SMKN 2 MAGELANG mengundang untuk menghadiri. Dengan Tujuan. Rapat ini akan diadakan pada: </font>
		       </td>
		    </tr>
		</table>
		<br>
		</table>
		<table>
			<tr class="text2">
				<td>Hari Tanggal</td>
				<td width="541">: <b></b></td>
                <td></td>
			</tr>
			<tr>
				<td>Jam</td>
				<td width="525">: </td>
                <td></td>
			</tr>
			<tr>
				<td>Tempat</td>
				<td width="525">:</td>
                <td></td>
			</tr>
		</table>
		<br>
		<table width="625">
			<tr>
		       <td>
			       <font size="2">Diharapkan atas kehadiranya, Demikian surat ini di sampaikan, atas perhatian dan kerjasamanya kami
ucapkan terima kasih.<br><br>Wassalamu'alaikum wr.wb.
</font>
		       </td>
		    </tr>
		</table>
		<br>
		<table width="625">
			<tr>
				<td width="430"><br><br><br><br></td>
				<td class="text" align="center">Kepala Sekolah<br><br><br><br>Nama<td></td></td>
			</tr>
	     </table>
	</center>
</body>
</html>

