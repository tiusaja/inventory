@extends('layouts.template')
@section('costume_css')
<link rel="stylesheet" href="assets/vendors/datatables.net-bs4/dataTables.boostrap4.css">
@endsection
@section('content')
<div class="row">
    <div class="col-md-12 grid-margin">
        <div class="d-flex justify-content-between flex-wrap">
            <div class="d-flex align-items-end flex-wrap">
                <div class="mr-md-3 mr-xl-5">
                    <h2>{{ $title }}</h2>
                    <div class="d-flex">
                        <i class="mdi mdi-home text-muted hover-cursor"></i>
                        <p class="text-muted mb-0 hover-cursor">&nbsp;/&nbsp;Surat Keluar&nbsp;/&nbsp;</p>
                        <p class="text-primary mb-0 hover-cursor">Buat Surat</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-12 grid-margin stretch-card">
        <div class="card">
            <div class="card-body mb-3">
                <h2 class="card-title">Form Tambah Surat</h2>
                <a href="/suratKeluar/daftarDinas" type="submit" class="btn btn-outline-danger m-2">Daftar Surat Dinas</a>
                <a href="/suratKeluar/daftarUndangan" type="submit" class="btn btn-outline-danger m-2">Daftar Surat Undangan</a>
                <a href="/suratKeluar/daftarKeputusan" type="submit" class="btn btn-outline-danger m-2">Daftar Surat Keputusan</a>
                <a href="/suratKeluar/daftarTugas" type="submit" class="btn btn-outline-danger m-2">Daftar Surat Tugas</a>
                <a href="/suratKeluar/daftarPengumuman" type="submit" class="btn btn-outline-danger m-2">Daftar Surat Pengumuman</a>
                <a href="/suratKeluar/daftarYayasan" type="submit" class="btn btn-outline-danger m-2">Daftar Surat Yayasan</a>
                <a href="/suratKeluar/daftarMou" type="submit" class="btn btn-outline-danger m-2">Daftar Surat MoU</a>
                <form class="row g-2 mb-3 mt-5">
                    <div class="col-md-6 mb-4">
                            <label for="pilih_kop">Pilih Kop Surat</label>
                            <select class="form-control border-dark" id="pilih_kop">
                                <option>SMKN 1 Magelang</option>
                                <option>SMKN 2 Magelang</option>
                                <option>SMKN 3 Magelang</option>
                            </select>
                    </div>
                    <div class="col-md-6 mb-4">
                        <label for="noSurat" class="visually-hidden">Nomor Surat</label>
                        <input type="integer" class="form-control border border-dark" id="noSurat">
                    </div>
                    <div class="col-md-6 mb-4">
                            <label for="pilih_kop">Pilih Kop Surat</label>
                            <select class="form-control border-dark" id="pilih_kop">
                                <option>SMKN 1 Magelang</option>
                                <option>SMKN 2 Magelang</option>
                                <option>SMKN 3 Magelang</option>
                            </select>
                    </div>
                    <div class="col-md-6 mb-4">
                        <label for="penetapan" class="visually-hidden">Perihal Penetapan</label>
                        <input type="text" class="form-control border-dark" id="penetapan" placeholder="Perihal Penetapan">
                    </div>
                    <div class="col-md-6 mb-4">
                        <label for="keputusan" class="visually-hidden">Perihal Keputusan</label>
                        <input type="text" class="form-control border-dark" id="keputusan" placeholder="Perihal Keputusan">
                    </div>
                    <div class="col-md-6 mb-4">
                        <label for="tempat" class="visually-hidden">Ditetapkan di :</label>
                        <input type="text" class="form-control border-dark" id="tempat" placeholder="Ditetapkan di">
                    </div>
                    <div class="col-md-6 mb-4">
                            <label for="penimbang">Perihal Penimbang</label>
                            <input type="text" class="form-control border-dark" id="penimbang" placeholder="Perihal Penimbang">
                    </div>
                    <div class="col-md-6 mb-4">
                        <label for="tanggal" class="visually-hidden">Ditetapkan Pada Tanggal</label>
                        <input type="date" class="form-control border-dark" id="tanggal" placeholder="Tanggal">
                    </div>
                    <div class="col-md-6 mb-4">
                        <label for="pengingat" class="visually-hidden">Perihal Pengingat</label>
                        <input type="text" class="form-control border-dark" id="pengingat" placeholder="Perihal Pengingat">
                    </div>
                    <div class="col-md-6 mb-4">

                    </div>
                    <div class="col-md-6">
                        <a href="/suratKeluar/keputusanCetak" type="submit" class="btn btn-primary mr-2">Submit</a>
                        <a href="" class="btn btn-danger">Cancel</a>
                    </div>
                </form>
            </div>
        </div>
    </div>


</div>
@endsection
