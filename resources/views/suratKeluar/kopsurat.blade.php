@extends('layouts.template')
@section('costume_css')
    <link rel="stylesheet" href="assets/vendors/datatables.net-bs4/dataTables.boostrap4.css">
@endsection
@section('content')
    <div class="row">
        <div class="col-md-12 grid-margin">
            <div class="d-flex justify-content-between flex-wrap">
                <div class="d-flex align-items-end flex-wrap">
                    <div class="mr-md-3 mr-xl-5">
                        <h2>{{ $title }}</h2>
                        <div class="d-flex">
                            <i class="mdi mdi-home text-muted hover-cursor"></i>
                            <p class="text-muted mb-0 hover-cursor">&nbsp;/&nbsp;Surat&nbsp;/&nbsp;</p>
                            <p class="text-primary mb-0 hover-cursor">Buat Kop Surat</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12 grid-margin stretch-card">
            <div class="card">
                <div class="card-body mb-3">
                    <h2 class="card-title">Buat Kop Surat</h2>
                    <form class="row g-2 mb-3 mt-5">
                        <div class="col-md-6 mb-4">
                            <label for="image" class="visually-hidden">Masukkan Gambar</label>
                            <input type="file" class="form-control border-dark" id="tanggal" placeholder="Tanggal">
                        </div>
                        <div class="col-md-6 mb-4">
                            <label for="instansi" class="visually-hidden">Instansi Pemerintah </label>
                            <input type="text" class="form-control border-dark" id="instansi" placeholder="Masukkan Nama Instansi Pemerintah">
                        </div>
                        <div class="col-md-6 mb-4">
                            <label for=kedinasan" class="visually-hidden">Kantor Kedinasan</label>
                            <input type="text" class="form-control border-dark" id="kedinasan" placeholder="Masukkan Kantor Kedinasan">
                        </div>
                        <div class="col-md-6 mb-4">
                        <div class="form-group">
                            <label for="sekolah">Nama Sekolah</label>
                            <input type="text" class="form-control border-dark" id="ditujukan" placeholder="Masukkan Nama Sekolah">
                        </div>
                        </div>
                        <div class="col-md-6 mb-4">
                            <label for="alamat" class="visually-hidden">Alamat Sekolah</label>
                            <input type="text" class="form-control border-dark" id="alamat" placeholder="Masukkan Alamat Sekolah ">
                        </div>
                        <div class="col-md-6 mb-4">
                            <label for="fax" class="visually-hidden">Nomor Fax / Telepon Instansi</label>
                            <input type="integer" class="form-control border-dark" id="fax" placeholder= "Masukkan Nomor / Fax">
                        </div>
                        <button type="submit" class="btn btn-primary mr-2">Submit</button>
                        <a href="/peminjaman/data_barang_peminjaman/create" class="btn btn-danger">Cancel</a>
                    </form>
                </div>
            </div>
        </div>


    </div>
@endsection
