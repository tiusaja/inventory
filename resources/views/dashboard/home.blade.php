@extends('layouts.template')

@section('content')
<div class="row">
    <div class="col-md-12 grid-margin">
        <div class="d-flex justify-content-between flex-wrap">
            <div class="d-flex align-items-end flex-wrap">
                <div class="mr-md-3 mr-xl-5">
                    <h2>Dashboard</h2>
                    <div class="d-flex">
                        <i class="mdi mdi-home text-muted hover-cursor"></i>
                        <p class="text-muted mb-0 hover-cursor">&nbsp;/&nbsp;Dashboard&nbsp;/&nbsp;</p>
                        <p class="text-primary mb-0 hover-cursor">Home</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-lg-12 grid-margin stretch-card">
        <div class="card">
            <div class="card d-inline" style="width: 100%; padding: 30px; border-Bottom: 25px solid #e7eaed; margin-Bottom: 50px">
                <img src="{{ asset('assets/images/tutWuri.png') }}" alt="logo" width="150px" style="float:left" />
                <div class="card-body" style="padding-Left: 180px; padding-Top:50px">
                    <h2>SMKN 2 KOTA MAGELANG</h2>
                    <h5 class="card-text">Alamat : Jalan Ahmad Yani No. 0 Kota Magelang</h5>
                </div>
            </div>
            <div class="card-body">
                <div class="table-responsive">
                    <table class="table table-hover table-bordered" id="datatables">
                        <thead>
                            <tr class="bg-dark text-light">
                                <th class="">No</th>
                                <th class="col-3">Email</th>
                                <th class="col-3">Password</th>
                                <th class="col-3">Posisi</th>
                                <th class="col-3">Aksi</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>1</td>
                                <td>69696969</td>
                                <td>Photoshop</td>
                                <td>Agus</td>
                                <td class="text-center">
                                    <a href="/peminjaman/data_barang_peminjaman/edit">
                                        <i class="fas fa-edit float-left"></i>
                                    </a>
                                    <a href="">
                                        <i class="fas fa-trash-alt text-danger"></i>
                                    </a>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>


</div>
@endsection

@section('costume_js')
<script src="assets/vendors/datatables.net/jquery.dataTables.js"></script>
<script src="assets/vendors/datatables.net-bs4/dataTables.bootstrap4.js"></script>
<script>
    $('#datatables').DataTable({
        scrollY: 500,
        scrollX: true,
        scrollcollapse: true,
        paging: false
    });
</script>
@endsection