@extends('layouts.template')

@section('costume_css')
<link rel="stylesheet" href="assets/vendors/datatables.net-bs4/dataTables.boostrap4.css">
@endsection

@section('content')
<div class="row">
    <div class="col-md-12 grid-margin">
        <div class="d-flex justify-content-between flex-wrap">
            <div class="d-flex align-items-end flex-wrap">
                <div class="mr-md-3 mr-xl-5">
                    <h2>{{ $title }}</h2>
                    <div class="d-flex">
                        <i class="mdi mdi-home text-muted hover-cursor"></i>
                        <p class="text-muted mb-0 hover-cursor">&nbsp;/&nbsp;User&nbsp;/&nbsp;</p>
                        <p class="text-primary mb-0 hover-cursor">Tambah User</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-10 grid-margin stretch-card">
        <div class="card">
            <div class="card-body">
                <h4 class="card-title">Form Tambah User</h4>
                <form class="forms-sample">
                    <div class="form-group">
                        <label for="email">Email</label>
                        <input type="email" class="form-control border-dark" id="email" placeholder="">
                    </div>
                    <div class="form-group">
                        <label for="password">Password</label>
                        <input type="password" class="form-control border-dark" id="password" placeholder="">
                    </div>
                    <div class="form-group">
                        <label for="ditujukan">Posisi</label>
                        <select class="form-control border-dark" id="posisi">
                            <option>Admin</option>
                            <option>Staff TU</option>
                        </select>
                    </div>
                    <button type="submit" class="btn btn-primary mr-2">Submit</button>
                    <a href="/inventory/data_asset" class="btn btn-danger">Cancel</a>
            </div>
            </form>
        </div>
    </div>
</div>
</div>


</div>
@endsection