@extends('layouts.template')
@section('content')
    <div class="row">
        <div class="col-md-12 grid-margin">
            <div class="d-flex justify-content-between flex-wrap">
                <div class="d-flex align-items-end flex-wrap">
                    <div class="mr-md-3 mr-xl-5">
                        <h2>{{ $title }}</h2>
                        <div class="d-flex">
                            <i class="mdi mdi-home text-muted hover-cursor"></i>
                            <p class="text-muted mb-0 hover-cursor">&nbsp;/&nbsp;Inventory&nbsp;/&nbsp;</p>
                            <p class="text-primary mb-0 hover-cursor">Data Kode Asset</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-6 grid-margin stretch-card">
            <div class="card">
                <div class="card-body">
                    <h4 class="card-title">Form Tambah Asset</h4>
                    <form class="forms-sample" action="{{ route('simpan_data_kode_asset') }}" method="post">
                        {{ csrf_field() }}
                        <div class="form-group">
                            <label for="data_kode_asset">Jenis Asset</label>
                            <select class="form-control" id="data_kode_asset" name="jenis_asset">
                                @foreach ($data_jenis_asset as $item)
                                    <option value="{{ $item->id }}">{{ $item->nama }}</option>
                                @endforeach
                            </select>
                            <div class="form-group">
                                <label for="nomor_asset">Nomor Asset</label>
                                <input type="number" class="form-control" id="nomor_asset" placeholder=""
                                    name="nomor_asset">
                            </div>
                            <div class="form-group">
                                <label for="nama_asset">Nama Asset</label>
                                <input type="text" class="form-control" id="nama_asset" placeholder="" name="nama_asset">
                            </div>
                            <div class="form-group">
                                <label for="kode_klasifikasi">Kode Klasifikasi</label>
                                <input type="text" class="form-control" id="kode_klasifikasi" placeholder=""
                                    name="kode_klasifikasi">
                            </div>
                            <div class="form-group">
                                <label for="kode_spesifikasi">Kode Spesifikasi</label>
                                <input type="text" class="form-control" id="kode_spesifikasi" placeholder=""
                                    name="kode_spesifikasi">
                            </div>
                            <button type="submit" class="btn btn-primary mr-2">Submit</button>
                            <a href="{{ route('data_kode_asset') }}" class="btn btn-danger">Cancel</a>
                        </div>
                    </form>
                </div>
            </div>
        </div>


    </div>
@endsection
