@extends('layouts.template')

@section('costume_css')
    <link rel="stylesheet" href="assets/vendors/datatables.net-bs4/dataTables.boostrap4.css">
@endsection

@section('content')
    <div class="row">
        <div class="col-md-12 grid-margin">
            <div class="d-flex justify-content-between flex-wrap">
                <div class="d-flex align-items-end flex-wrap">
                    <div class="mr-md-3 mr-xl-5">
                        <h2>{{ $title }}</h2>
                        <div class="d-flex">
                            <i class="mdi mdi-home text-muted hover-cursor"></i>
                            <p class="text-muted mb-0 hover-cursor">&nbsp;/&nbsp;Inventory&nbsp;/&nbsp;</p>
                            <p class="text-muted mb-0 hover-cursor">Data Asset&nbsp;/&nbsp;</p>
                            <p class="text-primary mb-0 hover-cursor">Edit Data</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-6 grid-margin stretch-card">
            <div class="card">
                <div class="card-body">
                    <h4 class="card-title">Form Edit Data Asset</h4>
                    <form class="forms-sample">
                        <div class="form-group">
                            <label for="data_kode_asset">Kode Asset</label>
                            <select class="form-control" id="data_kode_asset">
                                <option>1</option>
                                <option>2</option>
                                <option>3</option>
                                <option>4</option>
                                <option>5</option>
                            </select>
                            <div class="form-group">
                                <label for="nomor_asset">Nomor Asset</label>
                                <input type="number" class="form-control" id="nomor_asset" placeholder="">
                            </div>
                            <div class="form-group">
                                <label for="nama_asset">Nama Asset</label>
                                <input type="text" class="form-control" id="nama_asset" placeholder="">
                            </div>
                            <div class="form-group">
                                <label for="merek">Merek</label>
                                <input type="text" class="form-control" id="merek" placeholder="">
                            </div>
                            <div class="form-group">
                                <label for="lokasi">Lokasi</label>
                                <input type="text" class="form-control" id="lokasi" placeholder="">
                            </div>
                            <label for="kondisi">Kondisi</label>
                            <select class="form-control" id="kondisi">
                                <option>1</option>
                                <option>2</option>
                                <option>3</option>
                                <option>4</option>
                                <option>5</option>
                            </select>
                            <div class="form-group">
                                <label for="sumber_dana">Sumber Dana</label>
                                <input type="text" class="form-control" id="sumber_dana" placeholder="">
                            </div>
                            <div class="form-group">
                                <label for="sumber_dana">Sumber Dana</label>
                                <input type="text" class="form-control" id="sumber_dana" placeholder="">
                            </div>
                            <div class="form-group">
                                <label for="tahun_pembelian">Tahun Pembelian</label>
                                <input type="text" class="form-control" id="tahun_pembelian" placeholder="">
                            </div>
                            <div class="form-group">
                                <label for="umur_manfaat">Umur Manfaat (Tahun)</label>
                                <input type="text" class="form-control" id="umur_manfaat" placeholder="">
                            </div>
                            <div class="form-group">
                                <label for="jumlah">Jumlah</label>
                                <input type="text" class="form-control" id="jumlah" placeholder="">
                            </div>
                            <div class="form-group">
                                <label for="harga_satuan">Harga Satuan</label>
                                <input type="text" class="form-control" id="harga_satuan" placeholder="">
                            </div>
                            <button type="submit" class="btn btn-primary mr-2">Submit</button>
                            <a href="/inventory/data_asset" class="btn btn-danger">Cancel</a>
                        </div>
                    </form>
                </div>
            </div>
        </div>


    </div>
@endsection
