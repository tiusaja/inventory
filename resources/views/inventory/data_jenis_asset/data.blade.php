@extends('layouts.template')

@section('content')
    <div class="row">
        <div class="col-md-12 grid-margin">
            <div class="d-flex justify-content-between flex-wrap">
                <div class="d-flex align-items-end flex-wrap">
                    <div class="mr-md-3 mr-xl-5">
                        <h2>Data Jenis Asset</h2>
                        <div class="d-flex">
                            <i class="mdi mdi-home text-muted hover-cursor"></i>
                            <p class="text-muted mb-0 hover-cursor">&nbsp;/&nbsp;Inventory&nbsp;/&nbsp;</p>
                            <p class="text-primary mb-0 hover-cursor">Data Jenis Asset</p>
                        </div>
                    </div>
                </div>
                <div class="d-flex justify-content-between align-items-end flex-wrap">
                    <a href="{{ route('create_data_jenis_asset') }}" class="btn btn-primary mt-2 mt-xl-0">Buat Data</a>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-12 grid-margin stretch-card">
            <div class="card">
                <div class="card-body">
                    <h4 class="card-title">Data Jenis Asset</h4>
                    <div class="table-responsive">
                        <table class="table table-hover table-bordered data table-responsive" id="datatables">
                            <thead>
                                <tr class="bg-dark text-light">
                                    <th class="">No </th>
                                    <th class="col-6">Kode</th>
                                    <th class="col-5">Nama</th>
                                    <th class="col-1">Action</th>
                                </tr>
                            </thead>
                            <tbody>

                                @foreach ($data_jenis_asset as $data)
                                    <tr>
                                        <td>{{ $loop->iteration }}</td>
                                        <td>{{ $data->kode }}</td>
                                        <td>{{ $data->nama }}</td>
                                        <td class="text-center">
                                            <a href="{{ route('edit_data_jenis_asset', $data->id) }}">
                                                <i class="fas fa-edit float-left"></i>
                                            </a> |
                                            <a href="{{ route('delete_data_jenis_asset', $data->id) }}">
                                                <i class="fas fa-trash-alt text-danger float-right"></i>
                                            </a>
                                        </td>
                                    </tr>
                                @endforeach

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>

    </div>
@endsection

@section('costume_js')
    <script src="assets/vendors/datatables.net/jquery.dataTables.js"></script>
    <script src="assets/vendors/datatables.net-bs4/dataTables.bootstrap4.js"></script>
    <script>
        $('#datatables').DataTable({
            scrollY: 500,
            scrollX: true,
            scrollcollapse: true,
            paging: false
        });
    </script>
    @include('sweetalert::alert')

@endsection
