@extends('layouts.template')
@section('content')
    <div class="row">
        <div class="col-md-12 grid-margin">
            <div class="d-flex justify-content-between flex-wrap">
                <div class="d-flex align-items-end flex-wrap">
                    <div class="mr-md-3 mr-xl-5">
                        <h2>{{ $title }}</h2>
                        <div class="d-flex">
                            <i class="mdi mdi-home text-muted hover-cursor"></i>
                            <p class="text-muted mb-0 hover-cursor">&nbsp;/&nbsp;Inventory&nbsp;/&nbsp;</p>
                            <p class="text-primary mb-0 hover-cursor">Data Jenis Asset</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-6 grid-margin stretch-card">
            <div class="card">
                <div class="card-body">
                    <h4 class="card-title">Form Edit Jenis Asset</h4>
                    <form class="forms-sample" action="{{ route('update_data_jenis_asset', $edit_data->id) }}"
                        method="post">
                        {{ csrf_field() }}
                        <div class="form-group">
                            <label for="kode_jenis">Kode Jenis Asset</label>
                            <input type="text" class="form-control" id="kode_jenis" placeholder="Kode Jenis Asset"
                                name="kode" value="{{ $edit_data->kode }}">
                        </div>
                        <div class="form-group">
                            <label for="nama_jenis">Nama Jenis Asset</label>
                            <input type="text" class="form-control" id="nama_jenis" placeholder="" name="nama"
                                value="{{ $edit_data->nama }}">
                        </div>
                        <button type="submit" class="btn btn-primary mr-2">Simpan</button>
                        <a href="/inventory/data_jenis_asset" class="btn btn-danger">Cancel</a>
                    </form>
                </div>
            </div>
        </div>


    </div>
@endsection
