@extends('layouts.template')

@section('costume_css')
<link rel="stylesheet" href="assets/vendors/datatables.net-bs4/dataTables.boostrap4.css">
@endsection

@section('content')
<div class="row">
    <div class="col-md-12 grid-margin">
        <div class="d-flex justify-content-between flex-wrap">
            <div class="d-flex align-items-end flex-wrap">
                <div class="mr-md-3 mr-xl-5">
                    <h2>{{ $title }}</h2>
                    <div class="d-flex">
                        <i class="mdi mdi-home text-muted hover-cursor"></i>
                        <p class="text-muted mb-0 hover-cursor">&nbsp;/&nbsp;Sekolah&nbsp;/&nbsp;</p>
                        <p class="text-primary mb-0 hover-cursor">Tambah Sekolah</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-10 grid-margin stretch-card">
        <div class="card">
            <div class="card-body">
                <h4 class="card-title">Form Tambah Sekolah</h4>
                <form class="forms-sample">
                    <div class="form-group">
                        <label for="npsn">NPSN</label>
                        <input type="text" class="form-control border-dark" id="npsn" placeholder="">
                    </div>
                    <div class="form-group">
                        <label for="namaSekolah">Nama Sekolah</label>
                        <input type="text" class="form-control border-dark" id="namaSekolah" placeholder="">
                    </div>
                    <div class="form-group">
                        <label for="alamatSekolah">Alamat Sekolah</label>
                        <input type="text" class="form-control border-dark" id="alamatSekolah" placeholder="">
                    </div>
                    <div class="form-group">
                        <label for="noTelp">No Telepon Sekolah</label>
                        <input type="text" class="form-control border-dark" id="noTelp" placeholder="">
                    </div>
                    <div class="form-group">
                        <label for="email">Alamat Email</label>
                        <input type="email" class="form-control border-dark" id="email" placeholder="">
                    </div>
                    <div class="form-group">
                        <label for="kepsek">Nama Kepala Sekolah</label>
                        <input type="text" class="form-control border-dark" id="kepsek" placeholder="">
                    </div>
                    <div class="form-group">
                        <label for="noHp">Nomor HP Kepala Sekolah</label>
                        <input type="integer" class="form-control border-dark" id="noHp" placeholder="">
                    </div>
                    <div class="form-group">
                        <label for="jangka">Jangka Waktu Berlangganan</label>
                        <select class="form-control border-dark" id="jangka">
                            <option>3 bulan</option>
                            <option>6 bulan</option>
                            <option>9 bulan</option>
                            <option>12 bulan</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="tanggal">Tanggal Berlangganan</label>
                        <input type="date" class="form-control border-dark" id="tanggal" placeholder="">
                    </div>
                    <button type="submit" class="btn btn-primary mr-2">Submit</button>
                    <a href="/inventory/data_asset" class="btn btn-danger">Cancel</a>
            </div>
            </form>
        </div>
    </div>
</div>
</div>


</div>
@endsection