@extends('layouts.template')

@section('content')
<div class="row">
    <div class="col-md-12 grid-margin">
        <div class="d-flex justify-content-between flex-wrap">
            <div class="d-flex align-items-end flex-wrap">
                <div class="mr-md-3 mr-xl-5">
                    <h2>Daftar Sekolah</h2>
                    <div class="d-flex">
                        <i class="mdi mdi-home text-muted hover-cursor"></i>
                        <p class="text-muted mb-0 hover-cursor">&nbsp;/&nbsp;Sekolah&nbsp;/&nbsp;</p>
                        <p class="text-primary mb-0 hover-cursor">Daftar Sekolah</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-lg-12 grid-margin stretch-card">
        <div class="card">
            <div class="card-body">
                <h4 class="card-title">Daftar Sekolah</h4>
                <div class="table-responsive">
                    <table class="table table-hover table-bordered" id="datatables">
                        <thead>
                            <tr class="bg-dark text-light">
                                <th class="">No</th>
                                <th class="col-2">Nama Sekolah</th>
                                <th class="col-3">Tanggal Mulai Berlangganan</th>
                                <th class="col-2">Jangka Waktu</th>
                                <th class="col-3">Tanggal Akhir Berlangganan</th>
                                <th class="col-2">Aksi</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>1</td>
                                <td>69696969</td>
                                <td>Photoshop</td>
                                <td>Agus</td>
                                <td>29/10/2021</td>
                                <td class="text-center">
                                    <a href="/peminjaman/data_barang_peminjaman/edit">
                                        <i class="fas fa-edit float-left"></i>
                                    </a>
                                    <a href="">
                                        <i class="fas fa-trash-alt text-danger float-right"></i>
                                    </a>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>


</div>
@endsection

@section('costume_js')
<script src="assets/vendors/datatables.net/jquery.dataTables.js"></script>
<script src="assets/vendors/datatables.net-bs4/dataTables.bootstrap4.js"></script>
<script>
    $('#datatables').DataTable({
        scrollY: 500,
        scrollX: true,
        scrollcollapse: true,
        paging: false
    });
</script>
@endsection