@extends('layouts.template')
@section('content')
    <div class="row">
        <div class="col-md-12 grid-margin">
            <div class="d-flex justify-content-between flex-wrap">
                <div class="d-flex align-items-end flex-wrap">
                    <div class="mr-md-3 mr-xl-5">
                        <h2>Data Jenis Asset</h2>
                        <div class="d-flex">
                            <i class="mdi mdi-home text-muted hover-cursor"></i>
                            <p class="text-muted mb-0 hover-cursor">&nbsp;/&nbsp;Inventory&nbsp;/&nbsp;</p>
                            <p class="text-primary mb-0 hover-cursor">Data Jenis Asset</p>
                        </div>
                    </div>
                </div>
                <div class="d-flex justify-content-between align-items-end flex-wrap">
                    <a href="/create_data_jenis_asset" class="btn btn-primary mt-2 mt-xl-0">Buat Data</a>
                    {{-- <button class="btn btn-primary mt-2 mt-xl-0">Buat Data</button> --}}
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12 grid-margin stretch-card">
            <div class="card">
                <div class="card-body">
                    <h4 class="card-title">Data Jenis Asset</h4>
                    <div class="table-responsive">
                        <table class="table table-hover table-bordered">
                            <thead>
                                <tr class="bg-dark text-light">
                                    <th class="">No</th>
                                    <th class="col-6">Kode</th>
                                    <th class="col-5">Nama</th>
                                    <th class="col-1">Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>1</td>
                                    <td>69696969</td>
                                    <td>Photoshop</td>
                                    <td class="text-center">
                                        <a href="#">
                                            <i class="fas fa-edit float-left"></i>
                                        </a>
                                        <a href="">
                                            <i class="fas fa-trash-alt text-danger float-right"></i>
                                        </a>
                                    </td>
                                </tr>
                                <tr>
                                    <td>2</td>
                                    <td>6969</td>
                                    <td>Photoshop</td>
                                    <td class="text-center">
                                        <a href="#">
                                            <i class="fas fa-edit float-left"></i>
                                        </a>
                                        <a href="">
                                            <i class="fas fa-trash-alt text-danger float-right"></i>
                                        </a>
                                    </td>
                                </tr>
                                <tr>
                                    <td>3</td>
                                    <td>6969</td>
                                    <td>Photoshop</td>
                                    <td class="text-center">
                                        <a href="#">
                                            <i class="fas fa-edit float-left"></i>
                                        </a>
                                        <a href="">
                                            <i class="fas fa-trash-alt text-danger float-right"></i>
                                        </a>
                                    </td>
                                </tr>
                                <tr>
                                    <td>4</td>
                                    <td>6969</td>
                                    <td>Photoshop</td>
                                    <td class="text-center">
                                        <a href="#">
                                            <i class="fas fa-edit float-left"></i>
                                        </a>
                                        <a href="">
                                            <i class="fas fa-trash-alt text-danger float-right"></i>
                                        </a>
                                    </td>
                                </tr>
                                <tr>
                                    <td>5</td>
                                    <td>6969</td>
                                    <td>Photoshop</td>
                                    <td class="text-center">
                                        <a href="#">
                                            <i class="fas fa-edit float-left"></i>
                                        </a>
                                        <a href="">
                                            <i class="fas fa-trash-alt text-danger float-right"></i>
                                        </a>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>


    </div>
@endsection
