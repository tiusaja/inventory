@extends('layouts.template')
@section('content')
    <div class="row">
        <div class="col-md-12 grid-margin">
            <div class="d-flex justify-content-between flex-wrap">
                <div class="d-flex align-items-end flex-wrap">
                    <div class="mr-md-3 mr-xl-5">
                        <h2>Data Kode Asset</h2>
                        <div class="d-flex">
                            <i class="mdi mdi-home text-muted hover-cursor"></i>
                            <p class="text-muted mb-0 hover-cursor">&nbsp;/&nbsp;Inventory&nbsp;/&nbsp;</p>
                            <p class="text-primary mb-0 hover-cursor">Data Kode Asset</p>
                        </div>
                    </div>
                </div>
                <div class="d-flex justify-content-between align-items-end flex-wrap">
                    <a href="/create_data_kode_asset" class="btn btn-primary mt-2 mt-xl-0">Buat Data</a>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12 grid-margin stretch-card">
            <div class="card">
                <div class="card-body">
                    <h4 class="card-title">Data Kode Asset</h4>
                    <div class="table-responsive">
                        <table class="table table-hover table-bordered">
                            <thead>
                                <tr class="bg-dark text-light">
                                    <th class="">No</th>
                                    <th class="col-3">Kode</th>
                                    <th class="col-2">Nama</th>
                                    <th class="col-3">Kode Klasifikasi</th>
                                    <th class="col-3">Kode Spesifikasi</th>
                                    <th class="col-1">Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>1</td>
                                    <td>69696969</td>
                                    <td>Photoshop</td>
                                    <td>Photoshop</td>
                                    <td>Photoshop</td>
                                    <td class="text-center">
                                        <a href="#">
                                            <i class="fas fa-edit float-left"></i>
                                        </a>
                                        <a href="">
                                            <i class="fas fa-trash-alt text-danger float-right"></i>
                                        </a>
                                    </td>
                                </tr>
                                <tr>
                                    <td>1</td>
                                    <td>69696969</td>
                                    <td>Photoshop</td>
                                    <td>Photoshop</td>
                                    <td>Photoshop</td>
                                    <td class="text-center">
                                        <a href="#">
                                            <i class="fas fa-edit float-left"></i>
                                        </a>
                                        <a href="">
                                            <i class="fas fa-trash-alt text-danger float-right"></i>
                                        </a>
                                    </td>
                                </tr>
                                <tr>
                                    <td>1</td>
                                    <td>69696969</td>
                                    <td>Photoshop</td>
                                    <td>Photoshop</td>
                                    <td>Photoshop</td>
                                    <td class="text-center">
                                        <a href="#">
                                            <i class="fas fa-edit float-left"></i>
                                        </a>
                                        <a href="">
                                            <i class="fas fa-trash-alt text-danger float-right"></i>
                                        </a>
                                    </td>
                                </tr>
                                <tr>
                                    <td>1</td>
                                    <td>69696969</td>
                                    <td>Photoshop</td>
                                    <td>Photoshop</td>
                                    <td>Photoshop</td>
                                    <td class="text-center">
                                        <a href="#">
                                            <i class="fas fa-edit float-left"></i>
                                        </a>
                                        <a href="">
                                            <i class="fas fa-trash-alt text-danger float-right"></i>
                                        </a>
                                    </td>
                                </tr>
                                <tr>
                                    <td>1</td>
                                    <td>69696969</td>
                                    <td>Photoshop</td>
                                    <td>Photoshop</td>
                                    <td>Photoshop</td>
                                    <td class="text-center">
                                        <a href="#">
                                            <i class="fas fa-edit float-left"></i>
                                        </a>
                                        <a href="">
                                            <i class="fas fa-trash-alt text-danger float-right"></i>
                                        </a>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>


    </div>
@endsection
