@extends('layouts.template')
@section('content')
    <div class="row">
        <div class="col-md-12 grid-margin">
            <div class="d-flex justify-content-between flex-wrap">
                <div class="d-flex align-items-end flex-wrap">
                    <div class="mr-md-3 mr-xl-5">
                        <h2>Data Asset</h2>
                        <div class="d-flex">
                            <i class="mdi mdi-home text-muted hover-cursor"></i>
                            <p class="text-muted mb-0 hover-cursor">&nbsp;/&nbsp;Inventory&nbsp;/&nbsp;</p>
                            <p class="text-primary mb-0 hover-cursor">Data Asset</p>
                        </div>
                    </div>
                </div>
                <div class="d-flex justify-content-between align-items-end flex-wrap">
                    <a href="/create_data_asset" class="btn btn-primary mt-2 mt-xl-0">Buat Data</a>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12 grid-margin stretch-card">
            <div class="card">
                <div class="card-body">
                    <h4 class="card-title">Data Asset</h4>
                    <div class="table-responsive">
                        <table class="table table-hover table-bordered">
                            <thead>
                                <tr class="bg-dark text-light text-center">
                                    <th class="">No</th>
                                    <th class="col-2">Kode</th>
                                    <th class="col-2">Nama</th>
                                    <th class="">Merek</th>
                                    <th class="col-1">Lokasi Unit</th>
                                    <th class="">Kondisi Unit</th>
                                    <th class="">Tahun Pembelian</th>
                                    <th class="">Tafsiran Umur Manfaat (Tahun)</th>
                                    <th class="">Jumlah Unit</th>
                                    <th class="col-1">Harga per Unit</th>
                                    <th class="col-1">Total Harga</th>
                                    <th class="col-1">Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>1</td>
                                    <td>29195161</td>
                                    <td>Photoshop</td>
                                    <td>Photosoot</td>
                                    <td>Gudang</td>
                                    <td>Baru</td>
                                    <td>2019</td>
                                    <td>10</td>
                                    <td>20</td>
                                    <td>250.000</td>
                                    <td>5.000.000</td>
                                    <td class="text-center p-4">
                                        <a href="#">
                                            <i class="fas fa-edit float-left"></i>
                                        </a>
                                        <a href="">
                                            <i class="fas fa-trash-alt text-danger float-right"></i>
                                        </a>
                                    </td>
                                </tr>
                                <tr>
                                    <td>1</td>
                                    <td>29195161</td>
                                    <td>Photoshop</td>
                                    <td>Photosoot</td>
                                    <td>Gudang</td>
                                    <td>Baru</td>
                                    <td>2019</td>
                                    <td>10</td>
                                    <td>20</td>
                                    <td>250.000</td>
                                    <td>5.000.000</td>
                                    <td class="text-center p-4">
                                        <a href="#">
                                            <i class="fas fa-edit float-left"></i>
                                        </a>
                                        <a href="">
                                            <i class="fas fa-trash-alt text-danger float-right"></i>
                                        </a>
                                    </td>
                                </tr>
                                <tr>
                                    <td>1</td>
                                    <td>29195161</td>
                                    <td>Photoshop</td>
                                    <td>Photosoot</td>
                                    <td>Gudang</td>
                                    <td>Baru</td>
                                    <td>2019</td>
                                    <td>10</td>
                                    <td>20</td>
                                    <td>250.000</td>
                                    <td>5.000.000</td>
                                    <td class="text-center p-4">
                                        <a href="#">
                                            <i class="fas fa-edit float-left"></i>
                                        </a>
                                        <a href="">
                                            <i class="fas fa-trash-alt text-danger float-right"></i>
                                        </a>
                                    </td>
                                </tr>
                                <tr>
                                    <td>1</td>
                                    <td>29195161</td>
                                    <td>Photoshop</td>
                                    <td>Photosoot</td>
                                    <td>Gudang</td>
                                    <td>Baru</td>
                                    <td>2019</td>
                                    <td>10</td>
                                    <td>20</td>
                                    <td>250.000</td>
                                    <td>5.000.000</td>
                                    <td class="text-center p-4">
                                        <a href="#">
                                            <i class="fas fa-edit float-left"></i>
                                        </a>
                                        <a href="">
                                            <i class="fas fa-trash-alt text-danger float-right"></i>
                                        </a>
                                    </td>
                                </tr>
                                <tr>
                                    <td>1</td>
                                    <td>29195161</td>
                                    <td>Photoshop</td>
                                    <td>Photosoot</td>
                                    <td>Gudang</td>
                                    <td>Baru</td>
                                    <td>2019</td>
                                    <td>10</td>
                                    <td>20</td>
                                    <td>250.000</td>
                                    <td>5.000.000</td>
                                    <td class="text-center p-4">
                                        <a href="#">
                                            <i class="fas fa-edit float-left"></i>
                                        </a>
                                        <a href="">
                                            <i class="fas fa-trash-alt text-danger float-right"></i>
                                        </a>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>


    </div>
@endsection
