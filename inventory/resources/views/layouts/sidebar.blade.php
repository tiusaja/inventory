<nav class="sidebar sidebar-offcanvas" id="sidebar">
    <ul class="nav">
        <li class="nav-item {{ $title === 'Home' ? 'active' : '' }}">
            <a class="nav-link" href="/">
                <i class="mdi mdi-home menu-icon"></i>
                <span class="menu-title">Dashboard</span>
            </a>
        </li>
        <li class="nav-item">
            <a class="nav-link" data-toggle="collapse" href="#ui-basic" aria-expanded="false" aria-controls="ui-basic">
                <i class="mdi mdi-circle-outline menu-icon"></i>
                <span class="menu-title">Inventory</span>
                <i class="menu-arrow"></i>
            </a>
            <div class="collapse" id="ui-basic">
                <ul class="nav flex-column sub-menu">
                    <li class="nav-item {{ $title === 'Data Jenis Asset' ? 'active' : '' }}"> <a class="nav-link"
                            href="/data_jenis_asset">Data Jenis
                            Asset</a></li>
                    <li class="nav-item {{ $title === 'Data Code Asset' ? 'active' : '' }}"> <a class="nav-link"
                            href="/data_code_asset">Data Kode Asset</a></li>
                    <li class="nav-item {{ $title === 'Data Asset' ? 'active' : '' }}"> <a class="nav-link"
                            href="/data_asset">Data Asset</a></li>
                </ul>
            </div>
        </li>
    </ul>
</nav>
