<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('home', [
        "title" => "Home"
    ]);
});



// Todo ------------------------------------------- Data Assets

Route::get('/data_asset', function () {
    return view('inventory.data.data', [
        "title" => "Data Asset"
    ]);
});

Route::get('/create_data_asset', function () {
    return view('inventory.data.create', [
        "title" => "Tambah Data Asset"
    ]);
});


// Todo ------------------------------------------- Data Kode Assets

Route::get('/data_code_asset', function () {
    return view('inventory.data_code_asset.data', [
        "title" => "Data Code Asset"
    ]);
});

Route::get('/create_data_kode_asset', function () {
    return view('inventory.data_code_asset.create', [
        "title" => "Tambah Data Kode Asset"
    ]);
});


// Todo ------------------------------------------- Data Jenis Assets

Route::get('/data_jenis_asset', function () {
    return view('inventory.data_jenis_asset.data', [
        "title" => "Data Jenis Asset"
    ]);
});

Route::get('/create_data_jenis_asset', function () {
    return view('inventory.data_jenis_asset.create', [
        "title" => "Tambah Data Jenis Asset"
    ]);
});
