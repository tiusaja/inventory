<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class SuratDinasKeluar extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('surat_dinas_keluar', function (Blueprint $table) {
            $table->id();
            $table->integer('nomor_surat');
            $table->date('tanggal');
            $table->string('perihal');
            $table->string('lampiran');
            $table->string('ditujukan_kepada');
            $table->text('acara');
            $table->string('tujuan');
            $table->date('tanggal_acara');
            $table->time('waktu');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('surat_dinas_keluar');
    }
}
