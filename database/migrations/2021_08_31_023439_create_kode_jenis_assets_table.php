<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateKodeJenisAssetsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('kode_jenis_assets', function (Blueprint $table) {
            $table->id();
            $table->foreignId('jenis_asset_id')->constrained('data_jenis_assets')->onUpdate('cascade')->onDelete('cascade');
            $table->string('nama_asset');
            $table->string('jenis_asset')->nullable();
            $table->integer('nomor_asset');
            $table->string('kode_klasifikasi');
            $table->string('kode_spesifikasi');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('kode_jenis_assets');
    }
}
