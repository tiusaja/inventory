<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use App\Models\User;
use Illuminate\Support\Str;
use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function pagelogin()
    {
        return view('Auth.login', [
            "title" => "Login"
        ]);
    }


    public function postlogin(Request $request)
    {
        if (Auth::attempt($request->only('email', 'password'))) {
            return redirect('/dashboard');
        }
        return redirect('/');
    }

    public function logout()
    {
        Auth::logout();
        return redirect('/');
    }

    public function registrasi()
    {
        return view('Auth.register', [
            "title" => "Register"
        ]);
    }

    public function simpanregistrasi(Request $request)
    {
        // dd($request->all());

        User::create([
            'username' => $request->username,
            'level' => 'user',
            'email' => $request->email,
            'password' => bcrypt($request->password),
            'remember_token' => Str::random(45),
        ]);

        return view('home', [
            "title" => "Dashboard"
        ]);
    }
}
