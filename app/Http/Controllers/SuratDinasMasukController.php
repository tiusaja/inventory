<?php

namespace App\Http\Controllers;

use App\Models\Surat_Dinas_Masuk;
use Illuminate\Http\Request;

use SuratDinasMasuk;

class SuratDinasMasukController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    // public function dinasCetak()
    // {
    // 	$surat_dinas = surat_dinas::findOrFail();
    //     return view('suratKeluar.dinasCetak', compact('surat_dinas'));


    // 	foreach ($surat_dinas as $data) {
    // 		echo $data->nomor_surat . '<br>';
    //         echo $data->tanggal . '<br>';
    //         echo $data->perihal . '<br>';
    //         echo $data->lampiran . '<br>';
    //         echo $data->ditujukan_kepada . '<br>';
    //         echo $data->acara . '<br>';
    //         echo $data->tujuan . '<br>';
    //         echo $data->tanggal_acara . '<br>';
    //         echo $data->nomor_surat . '<br>';
    // 	}
    // }


    public function index()
    {
        $title = 'Daftar Surat';
        $surat_dinas_masuk = surat_dinas_masuk::all();
        return view('suratMasuk.daftarDinas', compact('surat_dinas_masuk', 'title'));
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
     {

         $surat_dinas_masuk = surat_dinas_masuk::all();
         return view('suratMasuk.suratDinas.create', compact('surat_dinas_masuk'), [
              "title" => "Surat Dinas  "
         ]);
     }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
            try {
                $surat_dinas_masuk = new surat_dinas_masuk();
                $surat_dinas_masuk->nomor_surat = $request->nomor_surat;
                $surat_dinas_masuk->tanggal = $request->tanggal;
                $surat_dinas_masuk->perihal = $request->perihal;
                $surat_dinas_masuk->lampiran = $request->lampiran;
                $surat_dinas_masuk->ditujukan_kepada = $request->ditujukan_kepada;
                $surat_dinas_masuk->acara = $request->acara;
                $surat_dinas_masuk->tujuan = $request->tujuan;
                $surat_dinas_masuk->tanggal_acara = $request->tanggal_acara;
                $surat_dinas_masuk->waktu = $request->waktu;

                $surat_dinas_masuk->save();

            } catch (\Throwable $th) {
                dd($th);
            }

            return redirect('/suratMasuk/daftarDinas');
     }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $surat_dinas_masuk = surat_dinas_masuk::findorfail($id);
        return view('suratMasuk.suratDinas.edit', compact('edit_data'), [
            "title" => "Edit Data"
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $update_data = surat_dinas_masuk::findorfail($id);
        $update_data->update($request->all());
        return redirect('suratMasuk/createDinas')->with('toast_success', 'Data Berhasil Diupdate');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $delete = surat_dinas_masuk::where($id);
        $delete->delete();

        return back()->with('toast_success', 'Data Berhasil Dihapus');
    }
}
