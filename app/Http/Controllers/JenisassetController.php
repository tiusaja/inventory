<?php

namespace App\Http\Controllers;

use App\Models\Jenis_asset;
use Illuminate\Http\Request;

class JenisassetController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data_jenis_asset = Jenis_asset::all();
        return view('inventory.data_jenis_asset.data', compact('data_jenis_asset'), [
            "title" => "Data Jenis Asset"
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('inventory.data_jenis_asset.create', [
            "title" => "Tambah Data Jenis Asset"
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // dd($request->all());
        Jenis_asset::create([
            'kode' => $request->kode,
            'nama' => $request->nama,
        ]);

        return redirect('inventory/data_jenis_asset')->with('toast_success', 'Data Berhasil Disimpan');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $edit_data = Jenis_asset::findorfail($id);
        return view('inventory.data_jenis_asset.edit', compact('edit_data'), [
            "title" => "Edit Data Jenis Asset"
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $update_data = Jenis_asset::findorfail($id);
        $update_data->update($request->all());
        return redirect('inventory/data_jenis_asset')->with('toast_success', 'Data Berhasil Diupdate');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $delete = Jenis_asset::find($id);
        $delete->delete();

        return back()->with('toast_success', 'Data Berhasil Dihapus');
    }
}
