<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\suratTugas;

class suratTugasController extends Controller
{
    public function create()
{
   return view('suratTugas.create');
}

public function store(Request $request)
{

  $request->validate([
    'nip' => 'required',
    'tempat' => 'required',
    'tanggal' => 'required',
    'jabatan' => 'required',
    'instansi' => 'required',
    'isi_surat' => 'required',
  ]);

  $input = $request->all();

  $suratTugas = suratTugas::create($input);

  return back()->with('success',' Post baru berhasil dibuat.');

   function index()
  {
     $suratTugas = suratTugas::all();

     return view('suratTugas.index', ['suratTugas' => $suratTugas]);
  }

   function edit($id)
  {
     $suratTugas = suratTugas::findOrFail($id);

     return view('suratTugas.edit', [
            'suratTugas' => $suratTugas
     ]);
   }

   function update(Request $request, $id)
  {
    $request->validate([
        'nip' => 'required',
        'tempat' => 'required',
        'tanggal' => 'required',
        'jabatan' => 'required',
        'instansi' => 'required',
        'isi_surat' => 'required',
    ]);

    $suratTugas = suratTugas::find($id)->update($request->all());

    return back()->with('success',' Data telah diperbaharui!');
  }

   function destroy($id)
{
   $suratTugas = suratTugas::find($id);

   $suratTugas->delete();

   return back()->with('success',' Penghapusan berhasil.');
}

}
}
