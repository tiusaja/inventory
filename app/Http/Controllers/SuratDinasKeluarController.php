<?php

namespace App\Http\Controllers;

use App\Models\Surat_Dinas_Keluar;
use Illuminate\Http\Request;

use SuratDinasKeluar;

class SuratDinasKeluarController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    // public function dinasCetak()
    // {
    // 	$surat_dinas = surat_dinas::findOrFail();
    //     return view('suratKeluar.dinasCetak', compact('surat_dinas'));


    // 	foreach ($surat_dinas as $data) {
    // 		echo $data->nomor_surat . '<br>';
    //         echo $data->tanggal . '<br>';
    //         echo $data->perihal . '<br>';
    //         echo $data->lampiran . '<br>';
    //         echo $data->ditujukan_kepada . '<br>';
    //         echo $data->acara . '<br>';
    //         echo $data->tujuan . '<br>';
    //         echo $data->tanggal_acara . '<br>';
    //         echo $data->nomor_surat . '<br>';
    // 	}
    // }


    public function index()
    {
        $title = 'Daftar Surat';
        $surat_dinas_keluar = surat_dinas_keluar::all();
        return view('suratKeluar.daftarDinas', compact('surat_dinas_keluar', 'title'));
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
     {

         $surat_dinas_keluar = surat_dinas_keluar::all();
         return view('suratKeluar.suratDinas.create', compact('surat_dinas_keluar'), [
              "title" => "Surat Dinas  "
         ]);
     }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
            try {
                $surat_dinas_keluar = new surat_dinas_keluar();
                $surat_dinas_keluar->nomor_surat = $request->nomor_surat;
                $surat_dinas_keluar->tanggal = $request->tanggal;
                $surat_dinas_keluar->perihal = $request->perihal;
                $surat_dinas_keluar->lampiran = $request->lampiran;
                $surat_dinas_keluar->ditujukan_kepada = $request->ditujukan_kepada;
                $surat_dinas_keluar->acara = $request->acara;
                $surat_dinas_keluar->tujuan = $request->tujuan;
                $surat_dinas_keluar->tanggal_acara = $request->tanggal_acara;
                $surat_dinas_keluar->waktu = $request->waktu;

                $surat_dinas_keluar->save();

            } catch (\Throwable $th) {
                dd($th);
            }

            return redirect('/suratKeluar/daftarDinas');
     }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $surat_dinas_keluar = surat_dinas_keluar::findorfail($id);
        return view('suratKeluar.suratDinas.edit', compact('edit_data'), [
            "title" => "Edit Data"
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $update_data = surat_dinas_keluar::findorfail($id);
        $update_data->update($request->all());
        return redirect('suratKeluar/createDinas')->with('toast_success', 'Data Berhasil Diupdate');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $delete = surat_dinas_keluar::where($id);
        $delete->delete();

        return back()->with('toast_success', 'Data Berhasil Dihapus');
    }
}
