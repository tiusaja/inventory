<?php

namespace App\Http\Controllers;

use App\Models\Kode_asset;
use App\Models\Jenis_asset;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class KodeassetController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $data_kode_asset = Kode_asset::all();
        return view('inventory.data_code_asset.data', compact('data_kode_asset'), [
            "title" => "Data Kode Asset"
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

        $data_jenis_asset = Jenis_asset::all();
        return view('inventory.data_code_asset.create', compact('data_jenis_asset'), [
            "title" => "Data Kode Asset"
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // dd($request->all());
        Kode_asset::create([
            'jenis_asset_id' => $request->jenis_asset_id,
            'jenis_asset' => $request->jenis_asset,
            'nomor_asset' => $request->nomor_asset,
            'nama_asset' => $request->nama_asset,
            'kode_klasifikasi' => $request->kode_klasifikasi,
            'kode_spesifikasi' => $request->kode_spesifikasi,
        ]);

        return redirect('inventory/data_kode_asset')->with('toast_success', 'Data Berhasil Disimpan');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $edit_data = Kode_asset::findorfail($id);
        return view('inventory.data_code_asset.edit', compact('edit_data'), [
            "title" => "Edit Data Kode Asset"
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $update_data = Kode_asset::findorfail($id);
        $update_data->update($request->all());
        return redirect('inventory/data_kode_asset')->with('toast_success', 'Data Berhasil Diupdate');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $delete = Kode_asset::find($id);
        $delete->delete();

        return back()->with('toast_success', 'Data Berhasil Dihapus');
    }
}
