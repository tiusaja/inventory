<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class suratTugas extends Model
{
    use HasFactory;

    protected $fillable = [
        'id',
        'nip',
        'tempat',
        'tanggal',
        'jabatan',
        'instansi',
        'isi_surat',
      ];

  }
