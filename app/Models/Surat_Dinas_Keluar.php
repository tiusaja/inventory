<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Surat_Dinas_Keluar extends Model
{
    use HasFactory;

    protected $fillable = [
      'id',
      'nomor_surat',
      'tanggal',
      'perihal',
      'lampiran',
      'ditujukan_kepada',
      'acara',
      'tujuan',
      'tanggal_acara',
      'waktu',
    ];

    protected $table='surat_dinas_keluar';

}
