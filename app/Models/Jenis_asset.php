<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Jenis_asset extends Model
{
    protected $table = 'data_jenis_assets';
    protected $primaryKey = 'id';
    protected $fillable = [
        'id',
        'kode',
        'nama'
    ];
}
