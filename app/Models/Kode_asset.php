<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Kode_asset extends Model
{
  protected $table = 'kode_jenis_assets';
  protected $primaryKey = 'id';
  protected $fillable = [
    'id',
    'jenis_asset_id',
    'nama_asset',
    'jenis_asset',
    'nomor_asset',
    'kode_klasifikasi',
    'kode_spesifikasi',
  ];

  public function jenis_asset1()
  {
    return $this->belongsTo(jenis_asset::class);
  }
}
