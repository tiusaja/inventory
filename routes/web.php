<?php

use App\Http\Controllers\AuthController;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\JenisassetController;
use App\Http\Controllers\KodeassetController;
use App\Http\Controllers\SuratDinasKeluarController;
use App\Http\Controllers\SuratDinasMasukController;

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/dashboard', function () {
//     return view('home', [
//         "title" => "Home"
//     ]);
// });



// Todo ------------------------------------------- Auth

route::get('/', [AuthController::class, 'pagelogin'])->name('login');
route::post('/postlogin', [AuthController::class, 'postlogin'])->name('postlogin');
route::get('/logout', [AuthController::class, 'logout'])->name('logout');
route::get('/register', [AuthController::class, 'registrasi'])->name('registrasi');
route::post('/dashboard', [AuthController::class, 'simpanregistrasi'])->name('simpanregistrasi');

Route::group(['middleware' => ['auth', 'level:admin,user']], function () {
    route::get('/dashboard', [HomeController::class, 'index'])->name('home');
});


// Todo ------------------------------------------- Data Assets

Route::get('/inventory/data_asset', function () {
    return view('inventory.data.data', [
        "title" => "Data Asset"
    ]);
});

Route::get('/inventory/data_asset/create', function () {
    return view('inventory.data.create', [
        "title" => "Tambah Data Asset"
    ]);
});

Route::get('/inventory/data_asset/edit', function () {
    return view('inventory.data.edit', [
        "title" => "Edit Data Jenis Asset"
    ]);
});


// Todo ------------------------------------------- Data Kode Assets

// Route::get('/inventory/data_kode_asset', function () {
//     return view('inventory.data_code_asset.data', [
//         "title" => "Data Code Asset"
//     ]);
// });

// Route::get('/inventory/data_kode_asset/create', function () {
//     return view('inventory.data_code_asset.create', [
//         "title" => "Tambah Data Kode Asset"
//     ]);
// });

Route::get('/inventory/data_kode_asset/edit', function () {
    return view('inventory.data_code_asset.edit', [
        "title" => "Edit Data Jenis Asset"
    ]);
});

// Route::get('/inventory/data_kode_asset', [KodeassetController::class, 'index'])->name('data_kode_asset');
// Route::get('/inventory/data_kode_asset/create', [KodeassetController::class, 'create'])->name('create_data_kode_asset');


Route::get('/inventory/data_kode_asset', [KodeassetController::class, 'index'])->name('data_kode_asset');
Route::get('/inventory/create/data_kode_asset', [KodeassetController::class, 'create'])->name('create_data_kode_asset');
Route::post('/inventory/simpan/data_kode_asset', [KodeassetController::class, 'store'])->name('simpan_data_kode_asset');
Route::get('/inventory/edit_data_kode_asset/{id}', [KodeassetController::class, 'edit'])->name('edit_data_kode_asset');
Route::post('/inventory/update_data_kode_asset/{id}', [KodeassetController::class, 'update'])->name('update_data_kode_asset');
Route::get('/inventory/delete_data_kode_asset/{id}', [KodeassetController::class, 'destroy'])->name('delete_data_kode_asset');





Route::resource('surat_dinas_keluar', SuratDinasKeluarController::class);



// Todo ------------------------------------------- Data Jenis Assets

Route::get('/dashboard/home', function () {
    return view('/dashboard/home', [
        "title" => "Dashboard"
    ]);
});


// Todo ------------------------------------------- Data Jenis Assets

Route::get('/inventory/data_jenis_asset', [JenisassetController::class, 'index'])->name('data_jenis_asset');
Route::get('/inventory/create/data_jenis_asset', [JenisassetController::class, 'create'])->name('create_data_jenis_asset');
Route::post('/inventory/simpan/data_jenis_asset', [JenisassetController::class, 'store'])->name('simpan_data_jenis_asset');
Route::get('/inventory/edit_data_jenis_asset/{id}', [JenisassetController::class, 'edit'])->name('edit_data_jenis_asset');
Route::post('/inventory/update_data_jenis_asset/{id}', [JenisassetController::class, 'update'])->name('update_data_jenis_asset');
Route::get('/inventory/delete_data_jenis_asset/{id}', [JenisassetController::class, 'destroy'])->name('delete_data_jenis_asset');


// Todo ------------------------------------------- Peminjaman

Route::get('/peminjaman/daftar_peminjaman', function () {
    return view('peminjaman.daftar', [
        "title" => "Daftar Peminjaman"
    ]);
});

Route::get('/peminjaman/data_barang_peminjaman', function () {
    return view('peminjaman.data', [
        "title" => "Data Barang Peminjaman"
    ]);
});

Route::get('/peminjaman/data_barang_peminjaman/create', function () {
    return view('peminjaman.create', [
        "title" => "Tambah Data Barang Peminjaman"
    ]);
});

Route::get('/peminjaman/data_barang_peminjaman/edit', function () {
    return view('peminjaman.edit', [
        "title" => "Edit Data Barang Peminjaman"
    ]);
});

// Todo ------------------------------------------- Surat Masuk

Route::get('/suratMasuk/createSurat', function () {
    return view('suratMasuk.createSurat', [
        "title" => "Buat Surat Dinas"
    ]);
});

Route::get('/suratMasuk.daftarDinas', function () {
    return view('suratMasuk.daftarDinas', [
        "title" => "Daftar Surat"
    ]);
});

Route::get('/suratMasuk/daftarKeputusan', function () {
    return view('suratMasuk.daftarKeputusan', [
        "title" => "Daftar Surat"
    ]);
});

Route::get('/suratMasuk/daftarMou', function () {
    return view('suratMasuk.daftarMou', [
        "title" => "Daftar Surat"
    ]);
});

Route::get('/suratMasuk/daftarPengumuman', function () {
    return view('suratMasuk.daftarPengumuman', [
        "title" => "Daftar Surat"
    ]);
});

Route::get('/suratMasuk/daftarTugas', function () {
    return view('suratMasuk.daftarTugas', [
        "title" => "Daftar Surat"
    ]);
});

Route::get('/suratMasuk/daftarUndangan', function () {
    return view('suratMasuk.daftarUndangan', [
        "title" => "Daftar Surat"
    ]);
});

Route::get('/suratMasuk/daftarYayasan', function () {
    return view('suratMasuk.daftarYayasan', [
        "title" => "Daftar Surat"
    ]);
});

Route::get('/suratMasuk/dinas', function () {
    return view('suratMasuk.dinas', []);
});

Route::get('/suratMasuk/undangan', function () {
    return view('suratMasuk.undangan', [
        "title" => "Buat Surat Undangan"
    ]);
});

Route::get('/suratMasuk/keputusan', function () {
    return view('suratMasuk.keputusan', [
        "title" => "Buat Surat Keputusan"
    ]);
});

Route::get('/suratMasuk/tugas', function () {
    return view('suratMasuk.tugas', [
        "title" => "Buat Surat Tugas"
    ]);
});

Route::get('/suratMasuk/pengumuman', function () {
    return view('suratMasuk.pengumuman', [
        "title" => "Buat Surat Pengumuman"
    ]);
});

Route::get('/suratMasuk/mou', function () {
    return view('suratMasuk.mou', [
        "title" => "Buat Surat MoU"
    ]);
});

Route::get('/suratMasuk/yayasan', function () {
    return view('suratMasuk.yayasan', [
        "title" => "Buat Surat Yayasan"
    ]);
});

Route::get('/suratMasuk/dinasCetak', function () {
    return view('suratMasuk.dinasCetak', []);
});

Route::get('/suratMasuk/mouCetak', function () {
    return view('suratMasuk.mouCetak', []);
});

Route::get('/suratMasuk/pengumumanCetak', function () {
    return view('suratMasuk.pengumumanCetak', []);
});

Route::get('/suratMasuk/tugasCetak', function () {
    return view('suratMasuk.tugasCetak', []);
});

Route::get('/suratMasuk/undanganCetak', function () {
    return view('suratMasuk.undanganCetak', []);
});

Route::get('/suratMasuk/keputusanCetak', function () {
    return view('suratMasuk.keputusanCetak', []);
});

Route::get('/suratMasuk/yayasanCetak', function () {
    return view('suratMasuk.yayasanCetak', []);
});

Route::post('/suratMasuk/saveSurat', [SuratDinasMasukController::class, 'store'])->name('suratMasuk.store');

Route::get('/suratMasuk/daftarDinas', [SuratDinasMasukController::class, 'index'])->name('suratMasuk.index');
Route::get('/suratMasuk/dinasCetak', [SuratDinasMasukController::class, 'dinasCetak'])->name('suratMasuk.dinasCetak');
Route::get('/suratMasuk/deleteSurat/{id}', [SuratDinasMasukController::class, 'destroy'])->name('suratMasuk.deleteSurat');




// Todo ------------------------------------------- Surat Keluar


Route::get('/suratKeluar/tugas', function () {
    return view('suratKeluar.tugas', []);
});

Route::get('/suratKeluar/daftarDinas', function () {
    return view('suratKeluar.daftarDinas', [
        "title" => "Daftar Surat"
    ]);
});

Route::get('/suratKeluar/daftarKeputusan', function () {
    return view('suratKeluar.daftarKeputusan', [
        "title" => "Daftar Surat"
    ]);
});

Route::get('/suratKeluar/daftarMou', function () {
    return view('suratKeluar.daftarMou', [
        "title" => "Daftar Surat"
    ]);
});

Route::get('/suratKeluar/daftarPengumuman', function () {
    return view('suratKeluar.daftarPengumuman', [
        "title" => "Daftar Surat"
    ]);
});

Route::get('/suratKeluar/daftarTugas', function () {
    return view('suratKeluar.daftarTugas', [
        "title" => "Daftar Surat"
    ]);
});

Route::get('/suratKeluar/daftarUndangan', function () {
    return view('suratKeluar.daftarUndangan', [
        "title" => "Daftar Surat"
    ]);
});

Route::get('/suratKeluar/daftarYayasan', function () {
    return view('suratKeluar.daftarYayasan', [
        "title" => "Daftar Surat"
    ]);
});

Route::get('/suratKeluar/createDinas', function () {
    return view('suratKeluar.createDinas', [
        "title" => "Buat Surat Dinas"
    ]);
});

Route::post('/suratKeluar/saveSurat', [SuratDinasKeluarController::class, 'store'])->name('suratKeluar.store');

Route::get('/suratKeluar/daftarDinas', [SuratDinasKeluarController::class, 'index'])->name('suratKeluar.index');
Route::get('/suratKeluar/dinasCetak', [SuratDinasKeluarController::class, 'dinasCetak'])->name('suratKeluar.dinasCetak');
Route::get('/suratKeluar/deleteSurat/{id}', [SuratDinasKeluarController::class, 'destroy'])->name('suratKeluar.deleteSurat');



Route::get('/suratKeluar/dinas', function () {
    return view('suratKeluar.dinas', []);
});

Route::get('/suratKeluar/undangan', function () {
    return view('suratKeluar.undangan', [
        "title" => "Buat Surat Undangan"
    ]);
});

Route::get('/suratKeluar/keputusan', function () {
    return view('suratKeluar.keputusan', [
        "title" => "Buat Surat Keputusan"
    ]);
});

Route::get('/suratKeluar/tugas', function () {
    return view('suratKeluar.tugas', [
        "title" => "Buat Surat Tugas"
    ]);
});

Route::get('/suratKeluar/pengumuman', function () {
    return view('suratKeluar.pengumuman', [
        "title" => "Buat Surat Pengumuman"
    ]);
});

Route::get('/suratKeluar/mou', function () {
    return view('suratKeluar.mou', [
        "title" => "Buat Surat MoU"
    ]);
});

Route::get('/suratKeluar/yayasan', function () {
    return view('suratKeluar.yayasan', [
        "title" => "Buat Surat Yayasan"
    ]);
});

Route::get('/suratKeluar/dinasCetak', function () {
    return view('suratKeluar.dinasCetak', []);
});

Route::get('/suratKeluar/mouCetak', function () {
    return view('suratKeluar.mouCetak', []);
});

Route::get('/suratKeluar/pengumumanCetak', function () {
    return view('suratKeluar.pengumumanCetak', []);
});

Route::get('/suratKeluar/tugasCetak', function () {
    return view('suratKeluar.tugasCetak', []);
});


Route::get('/suratKeluar/undanganCetak', function () {
    return view('suratKeluar.undanganCetak', []);
});

Route::get('/suratKeluar/keputusanCetak', function () {
    return view('suratKeluar.keputusanCetak', []);
});

Route::get('/suratKeluar/yayasanCetak', function () {
    return view('suratKeluar.yayasanCetak', []);
});

Route::get('/suratKeluar/editDinas', function () {
    return view('suratKeluar.editDinas', []);
});

// Todo ------------------------------------------- Disposisi

Route::get('/disposisi/daftarDisposisi', function () {
    return view('disposisi.daftarDisposisi', [
        "title" => "Daftar Disposisi"
    ]);
});

// Todo ------------------------------------------- sekolah

Route::get('/sekolah/createSekolah', function () {
    return view('sekolah.createSekolah', [
        "title" => "Tambah Sekolah"
    ]);
});

Route::get('/sekolah/daftarSekolah', function () {
    return view('sekolah.daftarSekolah', [
        "title" => "Daftar Sekolah"
    ]);
});

// Todo ------------------------------------------- User

Route::get('/user/createUser', function () {
    return view('user.createUser', [
        "title" => "Tambah User"
    ]);
});

Route::get('/user/daftarUser', function () {
    return view('user.daftarUser', [
        "title" => "Daftar User"
    ]);
});

// Todo ------------------------------------------- User

Route::get('/profile/setting', function () {
    return view('profile.setting', [
        "title" => "Pengaturan Profil"
    ]);
});
